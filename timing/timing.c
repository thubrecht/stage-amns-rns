#include <gmp.h>
#include <stdio.h>

#include "../convert.h"
#include "../integers.h"
#include "timing.h"

int main(void) {
  printf("Lancement des mesures...");

  // Initialisation
  printf("Initialisation...\n");
  gmp_randstate_t r_state;
  mpz_t A, B;

  mpz_inits(A, B, NULL);

  gmp_randinit_default(r_state);
  gmp_randseed_ui(r_state, get_seed());

  uint64_t timer = 0, mean_timer = 0, time_start = 0, time_end = 0;
  uint64_t a_timer = 0, mean_a_timer = 0, a_time_start = 0, a_time_end = 0;
  uint64_t n_ins = 0, mean_ins = 0, n_ins_start = 0, n_ins_end = 0;

  printf("\nOpération sur %d bits...\n", S_BASE);

  INT(S_BASE) a, b, c;
#ifdef T_DOUBLE
  INT(S_DOUBLE) d;
#endif

  /*------------------------- Mesure de la fonction -------------------------*/

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
#ifdef O_SINGLE
    c = T_FUNCTION(S_BASE)(a);
#else
#ifdef T_DOUBLE
    d = T_FUNCTION(S_BASE)(a, b);
#else
    c = T_FUNCTION(S_BASE)(a, b);
#endif
#endif
  }

  n_ins = rdpmc_instructions();
  printf("RDPMC : %lu\n", n_ins);

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de données
    mpz_urandomb(A, r_state, S_BASE - 1);
    mpz_urandomb(B, r_state, S_BASE - 1);

    a = FROM_MPZ(S_BASE)(A);
    b = FROM_MPZ(S_BASE)(B);

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
    c = T_FUNCTION(S_BASE)(a);
#else
#ifdef T_DOUBLE
        d = T_FUNCTION(S_BASE)(a, b);
#else
        c = T_FUNCTION(S_BASE)(a, b);
#endif
#endif
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
    c = T_FUNCTION(S_BASE)(a);
#else
#ifdef T_DOUBLE
        d = T_FUNCTION(S_BASE)(a, b);
#else
        c = T_FUNCTION(S_BASE)(a, b);
#endif
#endif
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
    c = T_FUNCTION(S_BASE)(a);
#else
#ifdef T_DOUBLE
        d = T_FUNCTION(S_BASE)(a, b);
#else
        c = T_FUNCTION(S_BASE)(a, b);
#endif
#endif
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n",
         NAME(T_FUNCTION(S_BASE)), mean_timer / (N_SAMPLES * 100),
         mean_a_timer / (N_SAMPLES * 100), mean_ins / (N_SAMPLES * 100),
         (double)mean_ins / (double)mean_a_timer);

  // Vidage de la mémoire
  mpz_clears(A, B, NULL);
  gmp_randclear(r_state);

  return 0;
}
