#ifndef _TIMING_H
#define _TIMING_H

#define _GNU_SOURCE

#define N_TESTS 500  /* Répetitions par jeu de données */
#define N_SAMPLES 75 /* Jeux de données différents */

#include <stdint.h>
#include <stdio.h>
#include <sys/syscall.h>
#include <unistd.h>

/* ****************************************************************************
 *                                                                            *
 *           Measurements procedures according to INTEL white paper           *
 *                                                                            *
 *      "How to benchmark code execution times on INTEL IA-32 and IA-64"      *
 *                                                                            *
 **************************************************************************** */

// ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Ne pas oublier de desactiver le turbo boost
// /bin/sh -c "echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo"
// pour fiabiliser la mesure

inline static uint64_t cpu_cycles_start(void) {
  uint32_t hi, lo;
  __asm__ __volatile__(
      "CPUID\n\t"
      "RDTSC\n\t"
      "mov %%edx, %0\n\t"
      "mov %%eax, %1\n\t"
      : "=r"(hi), "=r"(lo)
      :
      : "%rax", "%rbx", "%rcx", "%rdx");

  return ((uint64_t)lo) ^ (((uint64_t)hi) << 32);
}

inline static uint64_t cpu_cycles_stop(void) {
  uint32_t hi, lo;
  __asm__ __volatile__(
      "RDTSCP\n\t"
      "mov %%edx, %0\n\t"
      "mov %%eax, %1\n\t"
      "CPUID\n\t"
      : "=r"(hi), "=r"(lo)
      :
      : "%rax", "%rbx", "%rcx", "%rdx");

  return ((uint64_t)lo) ^ (((uint64_t)hi) << 32);
}

// rdpmc_instructions uses a "fixed-function" performance counter to return the
// count of retired instructions on
//       the current core in the low-order 48 bits of an unsigned 64-bit
//       integer.
//
inline static uint64_t rdpmc_instructions(void) {
  uint32_t a, d, c;

  c = (1 << 30);
  __asm__ __volatile__("rdpmc" : "=a"(a), "=d"(d) : "c"(c));

  return ((uint64_t)a) | (((uint64_t)d) << 32);
}

// rdpmc_actual_cycles uses a "fixed-function" performance counter to return the
// count of actual CPU core cycles
//       executed by the current core.  Core cycles are not accumulated while
//       the processor is in the "HALT" state, which is used when the operating
//       system has no task(s) to run on a processor core.
//
inline static uint64_t rdpmc_actual_cycles(void) {
  uint32_t a, d, c;

  c = (1 << 30) + 1;
  __asm__ volatile("rdpmc" : "=a"(a), "=d"(d) : "c"(c));

  return ((uint64_t)a) | (((uint64_t)d) << 32);
}

// rdpmc_reference_cycles uses a "fixed-function" performance counter to return
// the count of "reference" (or "nominal")
//       CPU core cycles executed by the current core.  This counts at the same
//       rate as the TSC, but does not count when the core is in the "HALT"
//       state.  If a timed section of code shows a larger change in TSC than in
//       rdpmc_reference_cycles, the processor probably spent some time in a
//       HALT state.
//
inline static uint64_t rdpmc_reference_cycles(void) {
  uint32_t a, d, c;

  c = (1 << 30) + 2;
  __asm__ volatile("rdpmc" : "=a"(a), "=d"(d) : "c"(c));

  return ((uint64_t)a) | (((uint64_t)d) << 32);
  ;
}

// Macros pour la taille et la fonction à tester
#ifndef S_BASE
#define S_BASE 64

#define S_DOUBLE 128
#endif

#define _APPLY(F, N) APPLY(F, N)
#define T_FUNCTION(N) _APPLY(N_FUNCTION, N)

#ifndef N_FUNCTION
#define N_FUNCTION mul
#define T_DOUBLE
#endif

// Génération d'une graine pour gmp_randseed_ui
inline static unsigned long get_seed() {
  unsigned long seed[1];

  FILE* src = fopen("/dev/random", "r");
  fread(seed, sizeof(long), 1, src);
  fclose(src);

  return seed[0];
}

// L'entrée pour les mesures de temps
int main(void);

#endif
