#include "convert.h"
#include "integers.h"

int64_t mpz_to_64(mpz_t a) {
  int64_t r[1];
  mpz_export(r, NULL, 1, sizeof(int64_t), 0, 0, a);
  return (mpz_sgn(a) == -1) ? neg_64(r[0]) : r[0];
}

int128_t mpz_to_128(mpz_t a) {
  int128_t r[1];
  mpz_export(r, NULL, 1, sizeof(int128_t), 0, 0, a);
  return (mpz_sgn(a) == -1) ? neg_128(r[0]) : r[0];
}

int256_t mpz_to_256(mpz_t a) {
  int256_t r[1];
  mpz_export(r, NULL, 1, sizeof(int256_t), 0, 0, a);
  return (mpz_sgn(a) == -1) ? neg_256(r[0]) : r[0];
}

int512_t mpz_to_512(mpz_t a) {
  int512_t r;
  mpz_export(&r, NULL, 1, sizeof(int512_t), 0, 0, a);
  return (mpz_sgn(a) == -1) ? neg_512(r) : r;
}

void mpz_from_64(mpz_t A, int64_t a) {
  if (a < 0) {
    mpz_set_si(A, -a);
    mpz_neg(A, A);
  } else {
    mpz_set_si(A, a);
  }
}

void mpz_from_128(mpz_t A, int128_t a) {
  int128_t _a[1];
  if (a.high > 0) {
    _a[0] = a;
    mpz_import(A, 1, -1, sizeof(int128_t), 0, 0, _a);
  } else {
    _a[0] = neg_128(a);
    mpz_import(A, 1, -1, sizeof(int128_t), 0, 0, _a);
    mpz_neg(A, A);
  }
}

void mpz_from_256(mpz_t A, int256_t a) {
  int256_t _a[1];
  if (a.h_high > 0) {
    _a[0] = a;
    mpz_import(A, 1, -1, sizeof(int256_t), 0, 0, _a);
  } else {
    _a[0] = neg_256(a);
    mpz_import(A, 1, -1, sizeof(int256_t), 0, 0, _a);
    mpz_neg(A, A);
  }
}
