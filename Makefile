# Makefile

main: main.c rns.o amns.o
	gcc -O3 -o main main.c rns.o amns.o -lgmp -lcrypto

main_old: main.c rns.o amns.o
	gcc -O3 -o main main_old.c rns.o amns.o -lgmp -lcrypto

test: test_rns.c rns.o tests.c
	gcc -O3 -o rns test_rns.c rns.o -lgmp

amns.o: amns.c amns.h structs_data.h common.h integers.a
	gcc -O3 -c amns.c -lgmp -L. -lintegers

rns.o: rns.c rns.h structs_data.h common.h
	gcc -O3 -c rns.c -lgmp

integers.a: integers.o convert.o
	ar rcs libintegers.a integers.o convert.o

convert.o: convert.h convert.c
	gcc -O3 -c convert.c

integers.o: integers.c integers.h
	gcc -O3 -c integers.c

tests: integers.a
	@$(MAKE) -C tests

timing: integers.a
	@$(MAKE) -C timing

clean:
	@$(MAKE) -C tests clean
	@$(MAKE) -C timing clean
	rm -f *.o main *.a

