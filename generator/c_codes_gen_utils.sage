def get_sign(k):
    if k < 0:
        return '-'
    else:
        return '+'


def is_power_of_two(k):
    if k <= 0:
        return False
    return ((k & (k - 1)) == 0)


# ~ -----------------------------------------------------------------------------------------------

def build_square_code(n, c, arch):
    abs_c = abs(c)
    abs_c_is_2pow = is_power_of_two(abs_c)
    tmp_part3 = [""] * n
    tmp_part2 = [""] * (n - 1)

    for i in range(n):
        for j in range(i):
            pos = i + j
            if pos < n:
                if tmp_part3[pos] == "":
                    tmp_part3[pos] = arch.mul(f"pa[{i}]", f"pa[{j}]")
                else:
                    tmp_part3[pos] = arch.b_add(tmp_part3[pos], arch.mul(f"pa[{i}]", f"pa[{j}]"))
            else:
                if tmp_part2[pos % n] == "":
                    tmp_part2[pos % n] = arch.mul(f"pa[{i}]", f"pa[{j}]")
                else:
                    tmp_part2[pos % n] = arch.b_add(tmp_part2[pos % n], arch.mul(f"pa[{i}]", f"pa[{j}]"))

    for i in range(n):
        if tmp_part3[i] != "":
            tmp_part3[i] = arch.b_shift_1(tmp_part3[i])

    for i in range(n, (2 * n) - 1):
        if tmp_part2[i % n] != '' and (i % 2 == 0):
            tmp_part2[i % n] = arch.b_shift_1(tmp_part2[i % n])

    for i in range(n):
        pos = i + i
        if pos < n:
            if tmp_part3[pos] == "":
                tmp_part3[pos] = arch.mul(f"pa[{i}]", f"pa[{i}]")
            else:
                tmp_part3[pos] = arch.b_add(tmp_part3[pos], arch.mul(f"pa[{i}]", f"pa[{i}]"))
        else:
            if tmp_part2[pos % n] == "":
                tmp_part2[pos % n] = arch.mul(f"pa[{i}]", f"pa[{i}]")
            else:
                tmp_part2[pos % n] = arch.b_add(tmp_part2[pos % n], arch.mul(f"pa[{i}]", f"pa[{i}]"))

    for i in range(n, (2 * n) - 1):
        if i % 2 == 0:
            if abs_c == 1:
                pass
            elif abs_c == 2:
                tmp_part2[i % n] = arch.b_shift_1(tmp_part2[i % n])
            elif abs_c_is_2pow:
                tmp_part2[i % n] = arch.b_shift_left(tmp_part2[i % n], int(log(abs_c, 2)))  # noqa: F821
            else:
                tmp_part2[i % n] = arch.s_mul(tmp_part2[i % n], abs_c)
        else:
            if abs_c == 1:
                tmp_part2[i % n] = arch.b_shift_1(tmp_part2[i % n])
            elif abs_c_is_2pow:
                tmp_part2[i % n] = arch.b_shift_left(tmp_part2[i % n], int(log(abs_c, 2)) + 1)  # noqa: F821
            else:
                tmp_part2[i % n] = arch.s_mul(tmp_part2[i % n], abs_c * 2)

        if c < 0:
            tmp_part2[i % n] = arch.b_neg(tmp_part2[i % n])

    result = ""

    for i in range(n - 1):
        result += f"    tmp_prod_result[{i}] = " + arch.b_add(tmp_part3[i], tmp_part2[i]) + ";\n"

    result += f"    tmp_prod_result[{n - 1}] = " + tmp_part3[n - 1] + ";\n"
    return result


def build_prod_code(n, c, arch):
    abs_c = abs(c)
    abs_c_is_2pow = is_power_of_two(abs_c)
    tmp_part1 = [""] * n
    tmp_part2 = [""] * (n - 1)

    for i in range(n):
        for j in range(n):
            pos = i + j
            if pos < n:
                if tmp_part1[pos] == "":
                    tmp_part1[pos] = arch.mul(f"pa[{i}]", f"pb[{j}]")
                else:
                    tmp_part1[pos] = arch.b_add(tmp_part1[pos], arch.mul(f"pa[{i}]", f"pb[{j}]"))
            else:
                if tmp_part2[pos % n] == "":
                    tmp_part2[pos % n] = arch.mul(f"pa[{i}]", f"pb[{j}]")
                else:
                    tmp_part2[pos % n] = arch.b_add(tmp_part2[pos % n], arch.mul(f"pa[{i}]", f"pb[{j}]"))

    for i in range(n - 1):
        if abs_c == 1:
            pass
        elif abs_c == 2:
            tmp_part2[i] = arch.b_shift_1(tmp_part2[i])
        elif abs_c_is_2pow:
            tmp_part2[i] = arch.b_shift_left(tmp_part2[i], int(log(abs_c, 2)))  # noqa: F821
        else:
            tmp_part2[i] = arch.s_mul(tmp_part2[i], abs_c)

        if c < 0:
            tmp_part2[i] = arch.b_neg(tmp_part2[i])

    result = ""
    for i in range(n - 1):
        result += f"    tmp_prod_result[{i}] = " + arch.b_add(tmp_part1[i], tmp_part2[i]) + ";\n"

    result += f"    tmp_prod_result[{n - 1}] = " + tmp_part1[n - 1] + ";\n"

    return result

# ~ -----------------------------------------------------------------------------------------------


def build_exactRedCoeff_interProd_code(n, lambd, arch):
    abs_c = abs(lambd)
    abs_c_is_2pow = is_power_of_two(abs_c)

    tmp_part1 = [""] * n
    tmp_part2 = [""] * (n - 1)

    for i in range(n):
        for j in range(n):
            pos = i + j
            if pos < n:
                if tmp_part1[pos] == "":
                    tmp_part1[pos] = arch.mul(f"rop[{i}]", f"poly_P0[{j}]")
                else:
                    tmp_part1[pos] = arch.b_add(tmp_part1[pos], arch.mul(f"rop[{i}]", f"poly_P0[{j}]"))
            else:
                if tmp_part2[pos % n] == "":
                    tmp_part2[pos % n] = arch.mul(f"rop[{i}]", f"poly_P0[{j}]")
                else:
                    tmp_part2[pos % n] = arch.b_add(tmp_part2[pos % n], arch.mul(f"rop[{i}]", f"poly_P0[{j}]"))

    for i in range(n - 1):
        if abs_c == 1:
            pass
        elif abs_c == 2:
            tmp_part2[i] = arch.b_shift_1(tmp_part2[i])
        elif abs_c_is_2pow:
            tmp_part2[i] = arch.b_shift_left(tmp_part2[i], int(log(abs_c, 2)))  # noqa: F821
        else:
            tmp_part2[i] = arch.s_mul(tmp_part2[i], abs_c)

        if lambd < 0:
            tmp_part2[i] = arch.b_neg(tmp_part2[i])

    result = "\n"

    for i in range(n - 1):
        if tmp_part1[i] != "" and tmp_part2[i] != "":
            result += f"    tmp[{i}] = " + arch.b_add(tmp_part1[i], tmp_part2[i]) + ";\n"
        elif tmp_part1[i] != "":
            result += f"    tmp[{i}] = " + tmp_part1[i] + ";\n"
        elif tmp_part2[i] != "":
            result += f"    tmp[{i}] = " + tmp_part2[i] + ";\n"

    result += f"    tmp[{n - 1}] = " + tmp_part1[n - 1] + ";\n"

    return result


# ~ -----------------------------------------------------------------------------------------------

def build_red_int_code(arch, n, mont_phi, c, lambda_mod_phi, red_int_coeff, neg_inv_ri_rep_coeff):

    result = "    //~ computation of: op*neg_inv_ri_rep_coeff mod((X^n - c), mont_phi)\n"
    result += build_redInt_tmpQ_prod_code(n, mont_phi, lambda_mod_phi, neg_inv_ri_rep_coeff, arch)

    result += "\n    //~ computation of: tmp_q*red_int_coeff mod(X^n - c)\n"
    result += build_redInt_tmpZero_prod_code(n, c, red_int_coeff, arch)

    result += "\n    //~ computation of: (op + tmp_zero)/mont_phi\n"
    result += build_redInt_lastStep_prod_code(n, arch)

    return result


# ~ important: 'lambda_mod_phi' is supposed > 0 and elements of 'neg_inv_ri_rep_coeff' are >= 0
def build_redInt_tmpQ_prod_code(n, mont_phi, lambda_mod_phi, neg_inv_ri_rep_coeff, arch):
    tmp_part1 = [""] * n
    tmp_part2 = [""] * (n - 1)
    need_lambda = False

    # ~ some adjustments, if needed
    neg_inv_ri_rep_coeff += [0] * (n - len(neg_inv_ri_rep_coeff))

    # ~ note: elements of 'neg_inv_ri_rep_coeff' are >= 0
    for i in range(n):
        for j in range(n):

            if neg_inv_ri_rep_coeff[j] == 0:
                continue
            elif neg_inv_ri_rep_coeff[j] == 1:
                tmp = f"({arch.sm_int})op[{i}].low"
            else:
                tmp = arch.u_scal(f"op[{i}].low", f"int_red_coeff_inv[{j}]")

            pos = i+j
            if pos < n:
                if tmp_part1[pos] == "":
                    tmp_part1[pos] = tmp
                else:
                    tmp_part1[pos] = arch.u_add(tmp_part1[pos], tmp)
            else:
                if tmp_part2[pos % n] == "":
                    tmp_part2[pos % n] = tmp
                else:
                    tmp_part2[pos % n] = arch.u_add(tmp_part2[pos % n], tmp)

    # note: lambda_mod_phi > 0
    for i in range(n - 2):
        if (tmp_part2[i] == "") or (lambda_mod_phi == 1):
            continue
        elif lambda_mod_phi == 2:
            tmp_part2[i] = arch.u_shift_1(tmp_part2[i])
        elif is_power_of_two(lambda_mod_phi):
            tmp_part2[i] = arch.shift_left(tmp_part2[i], int(log(lambda_mod_phi, 2)))  # noqa: F821
        else:
            need_lambda = True
            tmp_part2[i] = arch.u_scal(tmp_part2[i], "lambda_mod_phi")

    if (tmp_part2[n - 2] == "") or (lambda_mod_phi == 1):
        pass
    else:
        # tred = (neg_inv_ri_rep_coeff[n - 1] * lambda_mod_phi) % mont_phi
        tmp_part2[n - 2] = arch.u_scal(f"op[{n - 1}].low", "q_coeff")

    result = f"    {arch.sm_uint} q_coeff = " + arch.u_init((neg_inv_ri_rep_coeff[n - 1] * lambda_mod_phi) % mont_phi) + ";\n"
    if need_lambda:
        result += f"    {arch.sm_uint} lambda_mod_phi = {arch.u_init(lambda_mod_phi)};\n"

    for i in range(n - 1):
        if tmp_part1[i] == "" and tmp_part2[i] == "":
            continue
        elif tmp_part1[i] == "":
            result += f"    tmp_q[{i}] = " + tmp_part2[i] + ";\n"
        elif tmp_part2[i] == "":
            result += f"    tmp_q[{i}] = " + tmp_part1[i] + ";\n"
        else:
            result += f"    tmp_q[{i}] = " + arch.u_add(tmp_part1[i], tmp_part2[i]) + ";\n"
    if tmp_part1[n - 1] == "":
        pass
    else:
        result += f"    tmp_q[{n - 1}] = " + tmp_part1[n - 1] + ";\n"

    return result


def build_redInt_tmpZero_prod_code(n, c, red_int_coeff, arch):
    tmp_part1 = [""] * n
    tmp_part2 = [""] * (n - 1)
    tmp_partStart = [""] * n
    need_c = False

    # ~ some adjustments, if needed
    red_int_coeff += [0] * (n - len(red_int_coeff))

    abs_c = abs(c)
    c_sign = get_sign(c)

    for i in range(n):
        tmp_partStart[i] = 'tmp_zero['+str(i)+'] = '

    for i in range(n):
        for j in range(n):

            if red_int_coeff[j] == 0:
                continue
            if abs(red_int_coeff[j]) == 1:
                tmp = f"({arch.sm_int})tmp_q[{i}]"
            else:
                # note : phi=(1<<word_size) >= 2*n*abs_c*rho and rho > inf_norm(red_int_coeff),
                # so overflow shouldn't happen on target architecture.
                tmp = arch.mul(f"({arch.sm_int})tmp_q[{i}]", f"int_red_coeff_0[{j}]")

            if red_int_coeff[j] < 0:
                tmp = arch.b_neg(tmp)

            pos = i + j
            if pos < n:
                if tmp_part1[pos] == "":
                    tmp_part1[pos] = tmp
                else:
                    tmp_part1[pos] = arch.b_add(tmp_part1[pos], tmp)
            else:
                if tmp_part2[pos % n] == "":
                    tmp_part2[pos % n] = tmp
                else:
                    tmp_part2[pos % n] = arch.b_add(tmp_part2[pos % n], tmp)

    for i in range(n - 2):
        if tmp_part2[i] != "":
            if abs_c == 1:
                pass
            elif abs_c == 2:
                tmp_part2[i] = arch.b_shift_1(tmp_part2[i])
            else:
                need_c = True
                tmp_part2[i] = arch.s_mul(tmp_part2[i], "c")

            if c < 0:
                tmp_part2[i] = arch.b_neg(tmp_part2[i])

    if tmp_part2[n - 2] == "":
        pass
    else:
        # note : phi = (1 << word_size) >= 2 * n * abs_c * rho and rho > inf_norm(red_int_coeff),
        # so overflow shouldn't happen on target architecture.
        tmp_part2[n - 2] = arch.mul(f"({arch.sm_int})tmp_q[{n - 1}]", "z_coeff")
        if get_sign(red_int_coeff[n - 1]) != c_sign:
            tmp_part2[n - 2] = arch.b_neg(tmp_part2[n - 2])

    result = f"    {arch.sm_int} z_coeff = {arch.u_init(abs(red_int_coeff[n - 1]) * abs_c)};\n\n"
    if need_c:
        result += f"    {arch.sm_int} c = {arch.init(abs_c)};\n"

    for i in range(n - 1):
        if tmp_part1[i] == "" and tmp_part2[i] == "":
            continue
        elif tmp_part1 == "":
            result += f"    tmp_zero[{i}] = {tmp_part2[i]};\n"
        elif tmp_part2 == "":
            result += f"    tmp_zero[{i}] = {tmp_part1[i]};\n"
        else:
            result += f"    tmp_zero[{i}] = {arch.b_add(tmp_part1[i], tmp_part2[i])};\n"
    if tmp_part1[n - 1] == "":
        pass
    else:
        result += f"    tmp_zero[{n - 1}] = {tmp_part1[n - 1]};\n"

    return result


def build_redInt_lastStep_prod_code(n, arch):
    result = ""

    for i in range(n):
        tmp = f"    rop[{i}] = " + arch.b_add(f"op[{i}]", f"tmp_zero[{i}]") + ".high;\n"
        result += tmp

    return result
