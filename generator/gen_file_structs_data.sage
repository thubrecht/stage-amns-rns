def build_structs_data_file(dir_path, arch, poly_deg, rho_log2, nb_max_add, conv_P0, conv_P1, red_int_coeff, red_int_inv):
    with open(dir_path+"/structs_data.h", "w") as f:
        f.write("#include \"../integers.h\"\n\n")
        f.write("#include <gmp.h>\n\n")

        f.write("#ifndef STRUCTS_DATA\n")
        f.write("#define STRUCTS_DATA\n\n\n")

        f.write("//~ IMPORTANT : We take 'phi = 1 << WORD_SIZE'\n")
        f.write("#define WORD_SIZE " + str(arch.w_size) + "\n")
        f.write("#define POLY_DEG " + str(poly_deg) + "\n")
        f.write("#define NB_COEFF " + str(poly_deg+1) + "\n")
        f.write("#define NB_ADD_MAX " + str(nb_max_add) + "\n\n")

        f.write("#define RHO_LOG2 " + str(rho_log2) + "  // rho = 1 << RHO_LOG2.\n\n")

        f.write("//~ representations of the polynomials P0 and P1, used for conversion into the AMNS\n")
        f.write("extern " + arch.sm_int + " poly_P0[NB_COEFF];\n")
        f.write("extern " + arch.sm_int + " poly_P1[NB_COEFF];\n\n")

        f.write("//~ representations of polynomials Pi, for i=2,...,n-1\n")
        f.write("extern " + arch.sm_int + " polys_P[(NB_COEFF - 2)][NB_COEFF];\n\n")

        f.write("//~ representation of the coefficient used in the internal reduction\n")
        f.write("extern " + arch.sm_int + " int_red_coeff_0[NB_COEFF];\n\n")
        f.write("extern " + arch.sm_uint + " int_red_coeff_inv[NB_COEFF];\n\n")

        f.write("extern mpz_t modul_p;\n")
        f.write("extern mpz_t gama_pow[POLY_DEG];\n\n")

        f.write("#endif\n")


def build_constants_file(dir_path, arch, conv_P0, conv_P1, red_int_coeff, red_int_inv):
    with open(dir_path + "/constants.c", "w") as f:
        f.write("#include \"../integers.h\"\n\n")
        f.write("#include \"structs_data.h\"\n\n")

        f.write("//~ representations of the polynomials P0 and P1, used for conversion into the AMNS\n")
        f.write(arch.sm_int + " poly_P0[NB_COEFF] = {" + ", ".join([arch.init(x) for x in conv_P0]) + "};\n")
        f.write(arch.sm_int + " poly_P1[NB_COEFF] = {" + ", ".join([arch.init(x) for x in conv_P1]) + "};\n\n")

        f.write("//~ representation of the coefficient used in the internal reduction\n")
        f.write(arch.sm_int + " int_red_coeff_0[NB_COEFF] = {" + ", ".join([arch.init(abs(x)) for x in red_int_coeff]) + "};\n\n")
        f.write(arch.sm_uint + " int_red_coeff_inv[NB_COEFF] = {" + ", ".join([arch.u_init(abs(x)) for x in red_int_inv]) + "};\n\n")
