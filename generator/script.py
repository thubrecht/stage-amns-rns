load("amns_generator.sage")  # noqa: F821

w_sizes = [64, 128]

p_sizes = [256, 300, 350, 400, 450, 500, 512, 650, 700]

for p_size in p_sizes:
    p = random_prime(2 ** p_size, lbound=(2 ** (p_size - 1)))  # noqa: F821
    print(p)
    print(p.nbits(), p.is_prime())

    for w_size in w_sizes:
        n_min = (p_size // w_size) + 1
        n_max = n_min + 4

        a_lambda_max = 1 << 3  # 8

        t_root_max = 60  # Temps maximal pour la recherche de racines
        find_all_roots = True

        print(
            generate_amns_candidates_with_n_min_max(  # noqa: F821
                w_size, p, n_min, n_max, a_lambda_max, t_root_max, find_all_roots
            ),
            "\n",
        )
