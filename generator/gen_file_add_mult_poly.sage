def build_add_mult_poly_h_file(dir_path, arch, n, mont_phi, lambd, red_int_coeff, neg_inv_ri_rep_coeff):
    with open(dir_path + "/operations.h", "w") as f:
        f.write("#include \"../integers.h\"\n\n")
        f.write("#include \"structs_data.h\"\n\n")

        f.write("#ifndef POLY_MULT_ADD\n")
        f.write("#define POLY_MULT_ADD\n\n\n")

        f.write("void sub_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb);\n")
        f.write("void add_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb);\n")
        f.write("void neg_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *op);\n")
        f.write("void scalar_mult_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *op, " + arch.sm_int + " scalar);\n")
        f.write("void double_poly_coeffs(" + arch.sm_int + " *rop, " + arch.sm_int + " *op);\n")
        f.write("void lshift_poly_coeffs(" + arch.sm_int + " *rop, " + arch.sm_int + " *op, int nb_pos);\n\n")
        f.write("void mult_mod_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb);\n\n")
        f.write("void square_mod_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa);\n\n")

        f.write("__always_inline void internal_reduction(" + arch.sm_int + " *rop, " + arch.bg_int + " *op) {\n")
        f.write("    " + arch.sm_uint + " tmp_q[NB_COEFF];\n")
        f.write("    " + arch.bg_int + " tmp_zero[NB_COEFF];\n\n")
        f.write(build_red_int_code(arch, n, mont_phi, lambd, (lambd % mont_phi), red_int_coeff, neg_inv_ri_rep_coeff))
        f.write("}\n\n")

        f.write("void exact_coeffs_reduction(" + arch.sm_int + " *rop, " + arch.sm_int + " *op);\n\n")

        f.write("#endif\n\n")


def build_add_mult_poly_c_file(dir_path, n, mont_phi, lambd, arch, red_int_coeff, neg_inv_ri_rep_coeff):
    with open(dir_path + "/operations.c", "w") as f:
        f.write("#include \"../integers.h\"\n\n")
        f.write("#include \"structs_data.h\"\n\n")

        f.write("#include \"operations.h\"\n\n\n")

        f.write("void add_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.add("pa[j]", "pb[j]") + ";\n")
        f.write("}\n\n")

        f.write("void sub_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.sub("pa[j]", "pb[j]") + ";\n")
        f.write("}\n\n")

        f.write("void neg_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *op){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.neg("op[j]") + ";\n")
        f.write("}\n\n")

        f.write("//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.\n")
        f.write("void scalar_mult_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *op, " + arch.sm_int + " scalar){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.scal("scalar",  "op[j]") + ";\n")
        f.write("}\n\n")

        f.write("//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.\n")
        f.write("void double_poly_coeffs(" + arch.sm_int + " *rop, " + arch.sm_int + " *op){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.shift_1("op[j]") + ";\n")
        f.write("}\n\n")

        f.write("//~ assumes 'nb_pos' and/or coeffs of 'op' small enough to avoid an overflow.\n")
        f.write("void lshift_poly_coeffs(" + arch.sm_int + " *rop, " + arch.sm_int + " *op, int nb_pos){\n")
        f.write("    int j;\n")
        f.write("    for (j=0; j<NB_COEFF; j++)\n")
        f.write("        rop[j] = " + arch.shift_left("op[j]", "nb_pos") + ";\n")
        f.write("}\n\n")

        f.write("//~ Computes pa(X)*pb(X) mod(E)\n")
        f.write("void mult_mod_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa, " + arch.sm_int + " *pb){\n\n")
        f.write("    " + arch.bg_int + " tmp_prod_result[NB_COEFF];\n")
        f.write(build_prod_code(n, lambd, arch))  # noqa: F821
        f.write("\n    internal_reduction(rop, tmp_prod_result);\n")
        f.write("}\n\n")

        f.write("//~ Computes pa(X)^2 mod(E)\n")
        f.write("void square_mod_poly(" + arch.sm_int + " *rop, " + arch.sm_int + " *pa){\n\n")
        f.write("    " + arch.bg_int + " tmp_prod_result[NB_COEFF];\n")
        f.write(build_square_code(n, lambd, arch))  # noqa: F821
        f.write("\n    internal_reduction(rop, tmp_prod_result);\n")
        f.write("}\n\n")

        f.write("//~ performs the internal reduction on 'op' and puts the result in 'rop'\n")
        f.write("//~ IMPORTANT : We take 'mont_phi = 1 << WORD_SIZE', so operations modulo mont_phi are automatically done using the appropriate variable type.\n")
        f.write("extern inline void internal_reduction(" + arch.sm_int + " *rop, " + arch.bg_int + " *op);\n\n")

        f.write("void exact_coeffs_reduction(" + arch.sm_int + " *rop, " + arch.sm_int + " *op){\n\n")
        f.write("    int i;\n")
        f.write("    " + arch.bg_int + " tmp[NB_COEFF];\n\n")
        f.write("    for(i=0; i<NB_COEFF; i++)\n")
        f.write("        tmp[i] = " + arch.grow("op[i]") + ";\n")
        f.write("\n")
        f.write("    internal_reduction(rop, tmp);\n")
        f.write(build_exactRedCoeff_interProd_code(n, lambd, arch))  # noqa: F821
        f.write("\n")
        f.write("    internal_reduction(rop, tmp);\n")
        f.write("}\n\n")







