import os
load("c_codes_gen_utils.sage")  # noqa: F821
load("gen_file_main.sage")  # noqa: F821
load("gen_file_init_amns.sage")  # noqa: F821
load("gen_file_structs_data.sage")  # noqa: F821
load("gen_file_add_mult_poly.sage")  # noqa: F821
load("gen_file_useful_functs.sage")  # noqa: F821


# ~ assumes that polynomial representation form is P(X) = a_0 + ... + a_n.X^n = [a_0, ..., a_n].
# ~ assumes 'amns_data' is a list containning (in this order): [delta, n, E, rho_log2, gamma, M, M', conv_P0, conv_P1]
# ~ assumes 'target_archi_info' is an ArchitectureInformation object
# ~ WARNING : 'amns_data' is supposed generated for 'target_archi_info'


def build_amns_c_codes(p, amns_data, target_archi_info, p_num=0, amns_num=0):

    amnsData = list(amns_data)

    redExtPol_coeffs = amnsData[2]

    amnsData[2] = -redExtPol_coeffs[0]   # lambda

    gen_c_codes(p, amnsData, target_archi_info, p_num, amns_num)


def gen_c_codes(p, amns_data, arch, p_num, amns_num):

    word_size = arch.w_size

    nb_max_add = amns_data[0]
    n = amns_data[1]
    lambd = amns_data[2]
    rho_log2 = amns_data[3]
    gmm = amns_data[4]
    red_int_pol = amns_data[5]
    neg_inv_ri = amns_data[6]
    conv_P0 = amns_data[7]
    conv_P1 = amns_data[8]

    p = Integer(p)  # noqa: F821

    dir_name = f"p_{p.nbits()}_{p_num}_{n}_{lambd}__{amns_num}-{word_size}"
    dir_path = "c_codes/" + dir_name

    try:
        os.mkdir(dir_path)
    except FileExistsError:  # if this directory already exist
        # On supprime les fichiers
        for f in os.scandir(dir_path):
            os.remove(f)

    mont_phi = 1 << word_size

    build_makefile(dir_path)  # noqa: F821
    build_main_file(dir_path, arch)  # noqa: F821

    build_structs_data_file(dir_path, arch, (n-1), rho_log2, nb_max_add, conv_P0, conv_P1, red_int_pol, neg_inv_ri)  # noqa: F821
    build_constants_file(dir_path, arch, conv_P0, conv_P1, red_int_pol, neg_inv_ri)  # noqa: F821

    build_amns_init_h_file(dir_path)  # noqa: F821
    build_amns_init_c_file(dir_path, p, gmm, arch)  # noqa: F821

    build_add_mult_poly_h_file(dir_path, arch, n, mont_phi, lambd, red_int_pol, neg_inv_ri)  # noqa: F821
    build_add_mult_poly_c_file(dir_path, n, mont_phi, lambd, arch, red_int_pol, neg_inv_ri)  # noqa: F821

    build_useful_functs_h_file(dir_path, arch)  # noqa: F821
    build_useful_functs_c_file(dir_path, arch)  # noqa: F821
