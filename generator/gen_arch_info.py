class ArchitectureInformation:
    def __init__(self, w_size):
        self.w_size = w_size
        self.dw_size = w_size * 2
        self.sm_int = f"int{w_size}_t"
        self.sm_uint = f"uint{w_size}_t"
        self.bg_int = f"int{w_size * 2}_t"
        self.bg_uint = f"uint{w_size * 2}_t"

    def b_add(self, a, b):
        return f"add_{self.dw_size}({a}, {b})"

    def add(self, a, b):
        return f"add_{self.w_size}({a}, {b})"

    def u_add(self, a, b):
        return f"addu_{self.w_size}({a}, {b})"

    def sub(self, a, b):
        return f"sub_{self.w_size}({a}, {b})"

    def mul(self, a, b):
        return f"mul_{self.w_size}({a}, {b})"

    def s_mul(self, a, b):
        return f"muls_{self.dw_size}({a}, {b})"

    def b_neg(self, a):
        return f"neg_{self.dw_size}({a})"

    def neg(self, a):
        return f"neg_{self.w_size}({a})"

    def scal(self, a, b):
        return f"scal_{self.w_size}({a}, {b})"

    def u_scal(self, a, b):
        return f"scalu_{self.w_size}({a}, {b})"

    def shift_1(self, a):
        return f"shift_by_1_{self.w_size}({a})"

    def u_shift_1(self, a):
        return f"shift_by_1u_{self.w_size}({a})"

    def shift_left(self, a, n):
        return f"shift_l_{self.w_size}({a}, {n})"

    def b_shift_1(self, a):
        return f"shift_by_1_{self.dw_size}({a})"

    def b_shift_left(self, a, n):
        return f"shift_l_{self.dw_size}({a}, {n})"

    def zero(self):
        return f"zero_{self.w_size}()"

    def b_zero(self):
        return f"zero_{self.dw_size}()"

    def grow(self, a):
        return f"grow_{self.w_size}({a})"

    def init(self, value):
        if self.w_size == 128:
            return f"{{.low = {value % (2**64)}UL, .high = {value // (2**64)}L}}"

        return f"{value}L"

    def u_init(self, value):
        if self.w_size == 128:
            return f"{{.low = {value % (2**64)}UL, .high = {value // (2**64)}UL}}"

        return f"{value}UL"

    def c_mask(self, value):
        return f"create_mask_{self.w_size}({value})"

    def mask(self, a, b):
        return f"mask_{self.w_size}({a}, {b})"

    def low_mpz(self, a):
        return f"low_{self.w_size}({a})"

    def to_mpz(self, a, b):
        return f"mpz_from_{self.w_size}({a}, {b})"
