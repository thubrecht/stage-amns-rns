#include <stdint.h>

#ifndef __INTEGERS_H
#define __INTEGERS_H

/* 128 bit integers */
typedef union {
  struct {
    uint64_t low;
    uint64_t high;
  };
#ifdef __SIZEOF_INT128__
  unsigned __int128 full;
#endif
} uint128_t;

typedef union {
  struct {
    uint64_t low;  // Avec le complément à deux, la partie basse est non signée
    int64_t high;
  };
#ifdef __SIZEOF_INT128__
  __int128 full;
#endif
  uint128_t _unsigned;
} int128_t;

/* 256 bit integers */
typedef union {
  struct {
    uint64_t l_low;
    uint64_t l_high;
    uint64_t h_low;
    int64_t h_high;
  };
#ifdef __SIZEOF_INT128__
  struct {
    unsigned __int128 f_low;
    __int128 f_high;
  };
#endif
  struct {
    uint128_t low;
    int128_t high;
  };
} int256_t;

typedef union {
  struct {
    uint64_t l_low;
    uint64_t l_high;
    uint64_t h_low;
    uint64_t h_high;
  };
#ifdef __SIZEOF_INT128__
  struct {
    unsigned __int128 f_low;
    unsigned __int128 f_high;
  };
#endif
  struct {
    uint128_t low;
    uint128_t high;
  };
} uint256_t;

/* 512 bit integers */
typedef union {
  struct {
    uint64_t l0;
    uint64_t l1;
    uint64_t l2;
    uint64_t l3;
    uint64_t l4;
    uint64_t l5;
    uint64_t l6;
    int64_t l7;
  };
  int64_t values[8];
  struct {
    uint256_t low;
    int256_t high;
  };
  struct {
    uint128_t l_low;
    uint128_t l_high;
    uint128_t h_low;
    int128_t h_high;
  };
} int512_t;

typedef union {
  struct {
    uint64_t l0;
    uint64_t l1;
    uint64_t l2;
    uint64_t l3;
    uint64_t l4;
    uint64_t l5;
    uint64_t l6;
    uint64_t l7;
  };
  uint64_t values[8];
  struct {
    uint256_t low;
    uint256_t high;
  };
  struct {
    uint128_t l_low;
    uint128_t l_high;
    uint128_t h_low;
    uint128_t h_high;
  };
} uint512_t;

typedef union {
  struct {
    uint64_t l0;
    uint64_t l1;
    uint64_t l2;
    uint64_t l3;
    uint64_t l4;
    uint64_t l5;
    uint64_t l6;
    uint64_t l7;
    uint64_t l8;
    uint64_t l9;
    uint64_t l10;
    uint64_t l11;
    uint64_t l12;
    uint64_t l13;
    uint64_t l14;
    uint64_t l15;
  };
  struct {
    uint512_t low;
    int512_t high;
  };
  struct {
    uint256_t l_low;
    uint256_t l_high;
    uint256_t h_low;
    int256_t h_high;
  };
  int64_t values[16];
} int1024_t;

typedef struct {
  int64_t high;
  uint64_t mid;
  uint64_t low;
} int192_t;

/* #include <stdio.h> */

/* __always_inline void bin(uint64_t n) { */
/*   uint64_t i; */
/*   for (i = (uint64_t)1 << 63; i > 0; i = i / 2) */
/*     (n & i) ? printf("1") : printf("0"); */
/* } */
/* __always_inline void print_128(int128_t a) { */
/*   bin(a.high); */
/*   bin(a.low); */
/* } */

/* __always_inline void print_256(int256_t a) { */
/*   bin(a.h_high); */
/*   bin(a.h_low); */
/*   bin(a.l_high); */
/*   bin(a.l_low); */
/* } */

/* *************************** */
/* Operations on 64 bit values */
/* *************************** */

__always_inline int64_t neg_64(int64_t a) {
  return -a;
}

__always_inline int64_t add_64(int64_t a, int64_t b) {
  return a + b;
}

__always_inline uint64_t addu_64(uint64_t a, uint64_t b) {
  return a + b;
}

__always_inline int64_t sub_64(int64_t a, int64_t b) {
  return a - b;
}

__always_inline int128_t mul_64(int64_t a, int64_t b) {
  int128_t r;
#ifdef __SIZEOF_INT128__
  r.full = (__int128)a * (__int128)b;
#else
  r.low = (uint64_t)(uint32_t)a * (uint32_t)b;
  r.high = (a >> 32) * (b >> 32);
  int64_t _m1 = (a >> 32) * ((uint32_t)b);
  int64_t _m2 = (b >> 32) * ((uint32_t)a);
  uint64_t m = (uint64_t)(uint32_t)_m1 + (uint32_t)_m2;
  r.low += m << 32;
  r.high += +(_m1 >> 32) + (_m2 >> 32) + (r.low < (m << 32)) + (m >> 32);
#endif
  return r;
}

__always_inline uint128_t mulu_64(uint64_t a, uint64_t b) {
  uint128_t r;
#ifdef __SIZEOF_INT128__
  r.full = (unsigned __int128)a * (unsigned __int128)b;
#else
  r.low = (uint64_t)(uint32_t)a * (uint32_t)b;
  r.high = (a >> 32) * (b >> 32);
  uint64_t _m1 = (a >> 32) * ((uint32_t)b);
  uint64_t _m2 = (b >> 32) * ((uint32_t)a);
  uint64_t m = (uint64_t)(uint32_t)_m1 + (uint32_t)_m2;
  r.low += m << 32;
  r.high += (_m1 >> 32) + (_m2 >> 32) + (r.low < (m << 32)) + (m >> 32);
#endif
  return r;
}

__always_inline int64_t scal_64(int64_t a, int64_t b) {
  return a * b;
}

__always_inline uint64_t scalu_64(uint64_t a, uint64_t b) {
  return a * b;
}

__always_inline int128_t grow_64(int64_t a) {
  int128_t r;
  r.low = a;
  r.high = a >> 63;  // Extension du signe
  return r;
}

__always_inline char cmp_64(int64_t a, int64_t b) {
  return (a > b) - (a < b);
}

__always_inline char cmpu_64(uint64_t a, uint64_t b) {
  return (a > b) - (a < b);
}

__always_inline int64_t shift_by_1_64(int64_t a) {
  return a << 1;
}

__always_inline uint64_t shift_by_1u_64(uint64_t a) {
  return a << 1;
}

__always_inline int64_t shift_r_64(int64_t a, int n) {
  return a >> n;
}

__always_inline int64_t shift_l_64(int64_t a, int n) {
  return a << n;
}

__always_inline int64_t create_mask_64(int l) {
  return ((int64_t)1 << l) - 1;
}

__always_inline int64_t mask_64(int64_t a, int64_t m) {
  return a & m;
}

__always_inline int log2_64(int64_t a) {
  return 63 - __builtin_clzl(a);
}

__always_inline int64_t zero_64() {
  return (int64_t)0;
}

/* **************************** */
/* Operations on 128 bit values */
/* **************************** */

#ifdef __SIZEOF_INT128__
// Conversions between __int128 and int128_t
__always_inline __int128 to_int(int128_t a) {
  return a.full;
}

__always_inline int128_t from_int(__int128 a) {
  int128_t r;
  r.full = a;
  return r;
}
#endif

// Arithmetic operations
__always_inline int128_t neg_128(int128_t a) {
  int128_t r;
#ifdef __SIZEOF_INT128__
  r.full = -a.full;
#else
  r.low = -a.low;
  r.high = ~a.high + (r.low < ~a.low);
#endif
  return r;
}

__always_inline int128_t add_128(int128_t a, int128_t b) {
  int128_t r;
  r.low = a.low + b.low;
  r.high = a.high + b.high + (r.low < a.low);
  return r;
}

__always_inline uint128_t addu_128(uint128_t a, uint128_t b) {
  uint128_t r;
  r.low = a.low + b.low;
  r.high = a.high + b.high + (r.low < a.low);
  return r;
}

__always_inline int128_t sub_128(int128_t a, int128_t b) {
  return add_128(a, neg_128(b));
}

__always_inline uint128_t subu_128(uint128_t a, uint128_t b) {
  uint128_t r;
#ifdef __SIZEOF_INT128__
  r.full = a.full - b.full;
#else
  r = addu_128(a, *(uint128_t*)&neg_128(*(int128_t*)&b));
#endif
  return r;
}

inline int256_t mul_128(int128_t a, int128_t b) {
  int256_t r;
  r.high = mul_64(a.high, b.high);
  r.low = mulu_64(a.low, b.low);

  uint128_t _rm1 = mulu_64(a.high, b.low);
  _rm1.high -= (a.high >> 63) & b.low;
  uint128_t _rm2 = mulu_64(a.low, b.high);
  _rm2.high -= (b.high >> 63) & a.low;
  uint128_t _rm = addu_128(_rm1, _rm2);

  r.l_high += _rm.low;
  r.h_low += _rm.high + (r.l_high < _rm.low);
  r.h_high += (r.h_low < _rm.high) - (_rm.high < _rm1.high);

  return r;
}

__always_inline uint256_t mulu_128(uint128_t a, uint128_t b) {
  uint256_t r;
  r.high = mulu_64(a.high, b.high);
  r.low = mulu_64(a.low, b.low);

  uint128_t _rm1 = mulu_64(a.high, b.low);
  uint128_t _rm2 = mulu_64(a.low, b.high);

  uint128_t _rm = addu_128(_rm1, _rm2);

  r.l_high += _rm.low;
  r.h_low += _rm.high + (r.l_high < _rm.low);
  r.h_high += (r.h_low < _rm.high) + (_rm.high < _rm1.high);

  return r;
}

__always_inline int128_t muls_128(int128_t a, int64_t b) {
  int128_t r;
  r = mul_64(a.low, b);
  int64_t _m = a.high * b;
  r.high += _m;
  /* r.high -= (b >> 63) & a.high; */
  return r;
}

__always_inline uint128_t mulsu_128(uint128_t a, uint64_t b) {
  uint128_t r;
  r = mulu_64(a.low, b);
  r.high += a.high * b;
  return r;
}

__always_inline int128_t scal_128(int128_t a, int128_t b) {
  int128_t r = (int128_t)mulu_64(a.low, b.low);
  r.high += a.low * b.high + b.low * a.high;
  return r;
}

__always_inline uint128_t scalu_128(uint128_t a, uint128_t b) {
  uint128_t r = mulu_64(a.low, b.low);
  r.high += a.high * b.low + a.low * b.high;
  return r;
}

__always_inline char cmp_128(int128_t a, int128_t b) {
  char r = cmp_64(a.high, b.high);
  return r ? r : cmpu_64(a.low, b.low);
}

__always_inline char cmpu_128(uint128_t a, uint128_t b) {
  char r = cmpu_64(a.high, b.high);
  return r ? r : cmpu_64(a.low, b.low);
}

// Shifts
__always_inline int128_t shift_by_1_128(int128_t a) {
  int128_t r;
#ifdef __SIZEOF_INT128__
  r.full = a.full << 1;
#else
  r.low = (a.low << 1);
  r.high = a.high << 1 | (a.low >> 63);
#endif
  return r;
}

__always_inline uint128_t shift_by_1u_128(uint128_t a) {
  uint128_t r;
#ifdef __SIZEOF_INT128__
  r.full = a.full << 1;
#else
  r.low = (a.low << 1);
  r.high = a.high << 1 | (a.low >> 63);
#endif
  return r;
}

__always_inline int128_t shift_r_128(int128_t a, int n) {
  int128_t r;
#ifdef __SIZEOF_INT128__
  r.full = a.full >> n;
#else
#endif
  return r;
}

__always_inline int128_t shift_l_128(int128_t a, int n) {
  int128_t r;
#ifdef __SIZEOF_INT128__
  r.full = a.full << n;
#else
#endif
  return r;
}

__always_inline uint128_t shift_by_64_128(uint128_t a) {
  uint128_t r;
  r.high = 0;
  r.low = a.high;
  return r;
}

// Masks
__always_inline int128_t create_mask_128(int l) {
  int128_t m;
  m.high = (l & 64) ? (1 << ((int64_t)l & 63)) - 1 : 0;
  m.low = (l & 64) ? 0 : ((int64_t)1 << l);
  m.low--;
  return m;
}

__always_inline int128_t mask_128(int128_t a, int128_t m) {
  int128_t r;
  r.high = a.high & m.high;
  r.low = a.low & m.low;
  return r;
}

__always_inline uint128_t masku_128(uint128_t a, uint128_t m) {
  uint128_t r;
  r.high = a.high & m.high;
  r.low = a.low & m.low;
  return r;
}

// Miscelleanous
__always_inline int log2_128(int128_t a) {
  if (a.high) {
    return 127 - __builtin_clzl(a.high);
  }

  return 63 - __builtin_clzl(a.low);
}

__always_inline int128_t zero_128() {
  int128_t r;
  r.high = 0;
  r.low = 0;
  return r;
}

/* **************************** */
/* Operations on 256 bit values */
/* **************************** */

__always_inline int256_t grow_128(int128_t a) {
  int256_t r;
  r.l_low = a.low;
  r.l_high = a.high;
  r.h_high = a.high >> 63;  // Extension du signe
  r.h_low = a.high >> 63;   // Extension du signe
  return r;
}

__always_inline int256_t neg_256(int256_t a) {
  int256_t r;
  r.l_low = ~a.l_low + 1;
  r.l_high = ~a.l_high + (r.l_low < ~a.l_low);
  r.h_low = ~a.h_low + (r.l_high < ~a.l_high);
  r.h_high = ~a.h_high + (r.h_low < ~a.h_low);
  return r;
}

__always_inline int256_t add_256(int256_t a, int256_t b) {
  int256_t r;
  r.l_low = a.l_low + b.l_low;
  r.l_high = a.l_high + b.l_high + (r.l_low < a.l_low);
  r.h_low = a.h_low + b.h_low + (r.l_high < a.l_high);
  r.h_high = a.h_high + b.h_high + (r.h_low < a.h_low);

  return r;
}

__always_inline uint256_t addu_256(uint256_t a, uint256_t b) {
  uint256_t r;
  r.l_low = a.l_low + b.l_low;
  r.l_high = a.l_high + b.l_high + (r.l_low < a.l_low);
  r.h_low = a.h_low + b.h_low + (r.l_high < a.l_high);
  r.h_high = a.h_high + b.h_high + (r.h_low < a.h_low);

  return r;
}

int256_t sub_256(int256_t a, int256_t b);

inline uint256_t subu_256(uint256_t a, uint256_t b) {
  int256_t r = add_256(*(int256_t*)&a, *(int256_t*)&b);
  return *(uint256_t*)&r;
}

inline int512_t mul_256(int256_t a, int256_t b) {
  int512_t r;
  uint128_t _mask;
  r.high = mul_128(a.high, b.high);
  r.low = mulu_128(a.low, b.low);

  uint256_t _rm1 = mulu_128(*(uint128_t*)&a.high, b.low);
  _mask.high = a.h_high >> 63;
  _mask.low = a.h_high >> 63;
  _rm1.high = subu_128(_rm1.high, masku_128(b.low, _mask));

  uint256_t _rm2 = mulu_128(a.low, *(uint128_t*)&b.high);
  _mask.high = a.h_high >> 63;
  _mask.low = a.h_high >> 63;
  _rm1.high = subu_128(_rm2.high, masku_128(a.low, _mask));

  uint256_t _rm = addu_256(_rm1, _rm2);

  r.l2 += _rm.l_low;
  r.l3 += _rm.l_high + (r.l2 < _rm.l_low);
  r.l4 += _rm.h_low + (r.l3 < _rm.l_high);
  r.l5 += _rm.h_high + (r.l4 < _rm.h_low);
  r.l6 += (r.l5 < _rm.h_high) - (_rm.h_high < _rm1.h_high);
  return r;
}

inline uint512_t mulu_256(uint256_t a, uint256_t b) {
  uint512_t r;
  r.high = mulu_128(a.high, b.high);
  r.low = mulu_128(a.low, b.low);

  uint256_t _rm1 = mulu_128(a.high, b.low);
  uint256_t _rm2 = mulu_128(a.low, b.high);

  uint256_t _rm = addu_256(_rm1, _rm2);
  r.l2 += _rm.l_low;
  r.l3 += _rm.l_high + (r.l2 < _rm.l_low);
  r.l4 += _rm.h_low + (r.l3 < _rm.l_high);
  r.l5 += _rm.h_high + (r.l4 < _rm.h_low);
  r.l6 += (r.l5 < _rm.h_high) - (_rm.h_high < _rm1.h_high);

  return r;
}

char cmp_256(int256_t a, int256_t b);

__always_inline int256_t shift_by_1_256(int256_t a) {
  int256_t r;
  r.l_low = (a.l_low << 1);
  r.l_high = (a.l_high << 1) | (a.l_low >> 63);
  r.h_low = (a.h_low << 1) | (a.h_high >> 63);
  r.h_high = (a.h_high << 1) | (a.h_low >> 63);
  return r;
}

int256_t shift_r_256(int256_t a, int n);

__always_inline int256_t shift_l_256(int256_t a, int n) {
  int256_t r;
  r.h_high = (a.h_high << n) | (a.h_low >> (-n & 63));
  r.h_low = (a.h_low << n) | (a.l_high >> (-n & 63));
  r.l_high = (a.l_high << n) | (a.l_low << (-n & 63));
  r.l_low = (a.l_low << n);
  return r;
}

int256_t create_mask_256(int l);
int256_t mask_256(int256_t a, int256_t m);

inline uint256_t masku_256(uint256_t a, uint256_t m) {
  uint256_t r;
  r.high = masku_128(a.high, m.high);
  r.low = masku_128(a.low, m.low);
  return r;
}

int log2_256(int256_t a);

__always_inline int256_t zero_256() {
  int256_t r;
  r.l_low = 0;
  r.l_high = 0;
  r.h_low = 0;
  r.h_high = 0;
  return r;
}

/* **************************** */
/* Operations on 256 bit values */
/* **************************** */

inline int512_t neg_512(int512_t a) {
  int512_t r;
  r.l0 = ~a.l0 + 1;
  r.l1 = ~a.l1 + (r.l0 < ~a.l0);
  r.l2 = ~a.l2 + (r.l1 < ~a.l1);
  r.l3 = ~a.l3 + (r.l2 < ~a.l2);
  r.l4 = ~a.l4 + (r.l3 < ~a.l3);
  r.l5 = ~a.l5 + (r.l4 < ~a.l4);
  r.l6 = ~a.l6 + (r.l5 < ~a.l5);
  r.l7 = ~a.l7 + (r.l6 < ~a.l6);
  return r;
}

inline int512_t add_512(int512_t a, int512_t b) {
  int512_t r;
  r.l0 = a.l0 + b.l0;
  r.l1 = a.l1 + b.l1 + (r.l0 < a.l0);
  r.l2 = a.l2 + b.l2 + (r.l1 < a.l1);
  r.l3 = a.l3 + b.l3 + (r.l2 < a.l2);
  r.l4 = a.l4 + b.l4 + (r.l3 < a.l3);
  r.l5 = a.l5 + b.l5 + (r.l4 < a.l4);
  r.l6 = a.l6 + b.l6 + (r.l5 < a.l5);
  r.l7 = a.l7 + b.l7 + (r.l6 < a.l6);
  return r;
}

inline uint512_t addu_512(uint512_t a, uint512_t b) {
  uint512_t r;
  r.l0 = a.l0 + b.l0;
  r.l1 = a.l1 + b.l1 + (r.l0 < a.l0);
  r.l2 = a.l2 + b.l2 + (r.l1 < a.l1);
  r.l3 = a.l3 + b.l3 + (r.l2 < a.l2);
  r.l4 = a.l4 + b.l4 + (r.l3 < a.l3);
  r.l5 = a.l5 + b.l5 + (r.l4 < a.l4);
  r.l6 = a.l6 + b.l6 + (r.l5 < a.l5);
  r.l7 = a.l7 + b.l7 + (r.l6 < a.l6);
  return r;
}

inline int1024_t mul_512(int512_t a, int512_t b) {
  int1024_t r;
  uint256_t _mask;
  r.high = mul_256(a.high, b.high);
  r.low = mulu_256(a.low, b.low);

  uint512_t _rm1 = mulu_256(*(uint256_t*)&a.high, b.low);
  _mask.h_high = a.l7 >> 63;
  _mask.h_low = a.l7 >> 63;
  _mask.l_high = a.l7 >> 63;
  _mask.l_low = a.l7 >> 63;
  _rm1.high = subu_256(_rm1.high, masku_256(b.low, _mask));

  uint512_t _rm2 = mulu_256(a.low, *(uint256_t*)&b.high);
  _mask.h_high = b.l7 >> 63;
  _mask.h_low = b.l7 >> 63;
  _mask.l_high = b.l7 >> 63;
  _mask.l_low = b.l7 >> 63;
  _rm2.high = subu_256(_rm2.high, masku_256(a.low, _mask));

  uint512_t _rm = addu_512(_rm1, _rm2);

  r.l4 += _rm.l0;
  r.l5 += _rm.l1 + (r.l4 < _rm.l0);
  r.l6 += _rm.l2 + (r.l5 < _rm.l1);
  r.l7 += _rm.l3 + (r.l6 < _rm.l2);
  r.l8 += _rm.l4 + (r.l7 < _rm.l3);
  r.l9 += _rm.l5 + (r.l8 < _rm.l4);
  r.l10 += _rm.l6 + (r.l9 < _rm.l5);
  r.l11 += _rm.l7 + (r.l10 < _rm.l6);
  r.l12 += (r.l11 < _rm.l7);
  r.l13 += r.l12 < (r.l11 < _rm.l7);
  return r;
}

char cmp_512(int512_t a, int512_t b);

/* Macros */
#define _STR(N) #N
#define NAME(N) _STR(N)
#define _TYPE(N) int##N##_t
#define INT(N) _TYPE(N)

#define APPLY(F, N) F##_##N

#define NEG(N) APPLY(neg, N)
#define ADD(N) APPLY(add, N)
#define SUB(N) APPLY(sub, N)
#define MUL(N) APPLY(mul, N)
#define SHIFT_1(N) APPLY(shift_by_1, N)
#define SHIFT_R(N) APPLY(shift_r, N)
#define SHIFT_L(N) APPLY(shift_l, N)
#define C_MASK(N) APPLY(create_mask, N)
#define MASK(N) APPLY(mask, N)
#define LOG2(N) APPLY(log2, N)
#define ZERO(N) APPLY(zero, N)

#endif
