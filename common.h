/////////////////////////////////////////////////
// Global variables for intermediate results in
// AMNS arithmetic operation
/////////////////////////////////////////////////

#include "integers.h"
#include "structs_data.h"

#ifndef _COMMON_H
#define _COMMON_H

//~ used in poly prod and internal reduction
extern int128 tmp_prod_result[NB_COEFF];
extern int128_t tmp_prod_result_2[NB_COEFF];

//~ used in the internal reductions
extern char tmp_sign_coeff_poly[NB_COEFF];

//~ used in the the little internal reduction
extern int64_t tmp_red_int_poly_low[NB_COEFF];
extern int64_t tmp_red_int_poly_high[NB_COEFF];

//~ used in the total internal reduction
extern int64_t tmp_red_coeff_poly_high[NB_COEFF];
extern int128 tmp_red_coeff_poly_low[NB_COEFF];
extern int128_t tmp_red_coeff_poly_low_2[NB_COEFF];
extern uint64_t red_int_mask;

//~ The module p
extern mpz_t modul_p;

//~ Powers of gama for convertions : amns to int.
//~ Values are computed in "main.c"
//~ Note : gama[0] = gama.
extern mpz_t gama_pow[POLY_DEG];

//~ Representations of (2^k)^i in the amns for convertions : int to amns.
//~ Note : P[0] ~ 2^k in the amns.
//~ Also representations are : P(X) = a0 + ... + an.X^n = (a0, ..., an).
extern int64_t CONV_P[POLY_DEG][NB_COEFF];

#endif
