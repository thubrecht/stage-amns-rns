/* Conversions entre des entiers mpz_t et des entiers intN_t */

#include <gmp.h>
#include <stdint.h>
#include "integers.h"

#ifndef _CONVERT_H
#define _CONVERT_H

__always_inline int64_t low_64(mpz_t a) {
  if (a->_mp_size) {
    return a->_mp_d[0];
  }

  return 0;
}

__always_inline int128_t low_128(mpz_t a) {
  int128_t r;
  r.low = a->_mp_size ? a->_mp_d[0] : 0;
  r.high = a->_mp_size > 1 ? a->_mp_d[1] : 0;

  return r;
}

int64_t mpz_to_64(mpz_t a);
int128_t mpz_to_128(mpz_t a);
int256_t mpz_to_256(mpz_t a);
int512_t mpz_to_512(mpz_t a);
// int192_t mpz_to_192(mpz_t a);
// int394_t mpz_to_394(mpz_t a);

void mpz_from_64(mpz_t A, int64_t a);
void mpz_from_128(mpz_t A, int128_t a);
void mpz_from_256(mpz_t A, int256_t a);
void mpz_from_512(mpz_t A, int512_t a);
// mpz_t mpz_from_192(int192_t a);
// mpz_t mpz_from_394(int394_t a);

// Macros for conversion
#define FROM_MPZ(N) APPLY(mpz_to, N)
#define TO_MPZ(N) APPLY(mpz_from, N)

#endif
