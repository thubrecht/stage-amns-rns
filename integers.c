#include "integers.h"

/* Operations on 64 bit values */
extern inline int64_t neg_64(int64_t a);
extern inline int64_t add_64(int64_t a, int64_t b);
extern inline uint64_t addu_64(uint64_t a, uint64_t b);
extern inline int64_t sub_64(int64_t a, int64_t b);
extern inline int128_t mul_64(int64_t a, int64_t b);
extern inline uint128_t mulu_64(uint64_t a, uint64_t b);
extern inline int64_t scal_64(int64_t a, int64_t b);

extern inline char cmp_64(int64_t a, int64_t b);
extern inline char cmpu_64(uint64_t a, uint64_t b);

extern inline int64_t shift_by_1_64(int64_t a);
extern inline int64_t shift_r_64(int64_t a, int n);

extern inline int64_t create_mask_64(int l);
extern inline int64_t mask_64(int64_t a, int64_t m);

extern inline int log2_64(int64_t a);

/* Operations on 128 bit values */
#ifdef __native_128
extern inline __int128 to_int(int128_t a);
extern inline int128_t from_int(__int128 a);
#endif

extern inline int128_t neg_128(int128_t a);
extern inline int128_t add_128(int128_t a, int128_t b);
extern inline uint128_t addu_128(uint128_t a, uint128_t b);
extern inline int128_t sub_128(int128_t a, int128_t b);

extern inline int256_t mul_128(int128_t a, int128_t b);

/* uint256_t mulu_128(uint128_t a, uint128_t b) { */
/*   uint256_t r; */

/*   r.high = mulu_64(a.high, b.high); */
/*   r.low = mulu_64(a.low, b.low); */

/*   uint128_t _rm1 = mulu_64(a.high, b.low); */
/*   uint128_t _rm2 = mulu_64(a.low, b.high); */
/*   uint128_t _rm = addu_128(_rm1, _rm2); */

/*   r.l_high += _rm.low; */
/*   r.h_low += _rm.high + (r.l_high < _rm.low); */
/*   r.h_high += (r.h_low < _rm.high); */

/*   return r; */
/* } */

extern inline char cmp_128(int128_t a, int128_t b);
extern inline char cmpu_128(uint128_t a, uint128_t b);

extern inline int128_t shift_by_1_128(int128_t a);
extern inline int128_t shift_r_128(int128_t a, int n);

extern inline int128_t create_mask_128(int l);
extern inline int128_t mask_128(int128_t a, int128_t m);

extern inline int log2_128(int128_t a);

/* Operations on 256 bit values */

extern inline int256_t neg_256(int256_t a);
extern inline int256_t add_256(int256_t a, int256_t b);

int256_t sub_256(int256_t a, int256_t b) {
  return add_256(a, neg_256(b));
}

extern inline int512_t mul_256(int256_t a, int256_t b);

// TODO : Mul
/* int512_t mul_256(int256_t a, int256_t b) { */
/*   int512_t r; */

/*   r.high = mul_128(a.high, b.high); */
/*   r.low = mulu_128(a.low, b.low); */

/*   // On utilise l'algo de Karatsuba */
/*   uint128_t _a, _b; */
/*   if (cmp_128) */
/* } */

char cmp_256(int256_t a, int256_t b) {
  char r = cmp_64(a.h_high, b.h_high);
  if (r) {
    return r;
  }
  r = cmpu_64(a.h_low, b.h_low);
  if (r) {
    return r;
  }
  r = cmpu_64(a.l_high, b.l_high);
  if (r) {
    return r;
  }
  return cmpu_64(a.l_low, b.l_low);
}

extern inline int256_t shift_by_1_256(int256_t a);

int256_t shift_r_256(int256_t a, int n) {
  int256_t r;
  r.h_high = (a.h_high >> n) | (a.h_high << (-n & 63));
  r.h_low = (a.h_low >> n) | (a.h_low << (-n & 63));
  r.l_high = (a.l_high >> n) | (a.l_high << (-n & 63));
  r.l_low = (a.l_low >> n) | (r.l_high & (-1 << (-n & 63)));
  r.l_high = (r.l_high & (-1 >> n)) | (r.h_low & (-1 << (-n & 63)));
  r.h_low = (r.h_low & (-1 >> n)) | (r.h_high & (-1 << (-n & 63)));
  r.h_high = (r.l_high & (-1 >> n));
  return r;
}

extern inline int256_t shift_l_256(int256_t a, int n);

int256_t create_mask_256(int l) {
  int256_t m;
  if (l & 192) {
    m.h_high = (1 << l & 63) - 1;
    m.h_low--;
    m.l_high--;
    m.l_low--;
  } else if (l & 128) {
    m.h_low = (1 << l & 63) - 1;
    m.l_high--;
    m.l_low--;
  } else if (l & 64) {
    m.l_high = (1 << l & 63) - 1;
    m.l_low--;
  } else {
    m.l_low = (1 << l & 63) - 1;
  }
  return m;
}

int256_t mask_256(int256_t a, int256_t m) {
  int256_t r;
  r.l_low = a.l_low & m.l_low;
  r.l_high = a.l_high & m.l_high;
  r.h_low = a.h_low & m.h_low;
  r.h_high = a.h_high & m.h_high;
  return r;
}

int log2_256(int256_t a) {
  if (a.h_high) {
    return 255 - __builtin_clzl(a.h_high);
  }

  if (a.h_low) {
    return 191 - __builtin_clzl(a.h_low);
  }

  if (a.l_high) {
    return 127 - __builtin_clzl(a.l_high);
  }

  return 63 - __builtin_clzl(a.l_low);
}

extern inline int256_t zero_256();

/* 512 bit functions */
extern inline int512_t add_512(int512_t a, int512_t b);

char cmp_512(int512_t a, int512_t b) {
  char r;
  for (char i = 0; i < 8; i++) {
    r = cmpu_64(a.values[i], b.values[i]);
    if (r) {
      break;
    }
  }

  return r;
}
