#include "structs_data.h"

#define _GNU_SOURCE

#include <stdio.h>
#include <sys/syscall.h>
#include <unistd.h>

#define NTEST 5000   // nombre de fois ou on repete le meme jeu de donnees
#define NSAMPLES 70  // nombre differents de jeu de donnees

inline uint64_t cpucyclesStart(void);
inline uint64_t cpucyclesStop(void);
inline unsigned long rdpmc_instructions(void);
