#include "amns.h"
#include <stdlib.h>
#include "integers.h"

#include "common.h"
//~ used in poly prod and internal reduction
int128 tmp_prod_result[NB_COEFF];
int128_t tmp_prod_result_2[NB_COEFF];
//~ used in the internal reductions
char tmp_sign_coeff_poly[NB_COEFF];
//~ used in the the little internal reduction
int64_t tmp_red_int_poly_low[NB_COEFF];
int64_t tmp_red_int_poly_high[NB_COEFF];
//~ used in the total internal reduction
int64_t tmp_red_coeff_poly_high[NB_COEFF];
int128 tmp_red_coeff_poly_low[NB_COEFF];
int128_t tmp_red_coeff_poly_low_2[NB_COEFF];

uint64_t red_int_mask;
//~ The modulus p
mpz_t modul_p;
//~ Powers of gama for convertions : amns to int.
//~ Values are computed in "main.c"
//~ Note : gama[0] = gama.
mpz_t gama_pow[POLY_DEG];
//~ Representations of (2^k)^i in the amns for convertions : int to amns.
//~ Note : P[0] ~ 2^k in the amns.
//~ Also representations are : P(X) = a0 + ... + an.X^n = (a0, ..., an).
extern int64_t CONV_P[POLY_DEG][NB_COEFF];

void add_poly(int64_t* rop, int64_t* pa, int64_t* pb) {
  int j;
  for (j = 0; j < NB_COEFF; j++)
    rop[j] = pa[j] + pb[j];
}

void sub_poly(int64_t* rop, int64_t* pa, int64_t* pb) {
  int j;
  for (j = 0; j < NB_COEFF; j++)
    rop[j] = pa[j] - pb[j];
}

void neg_poly(int64_t* rop, int64_t* op) {
  int j;
  for (j = 0; j < NB_COEFF; j++)
    rop[j] = -op[j];
}

//~ We assume the scalar enough small to avoid an overflow.
void scalar_mult_poly(int64_t* rop, int64_t* op, int64_t scalar) {
  int j;
  for (j = 0; j < NB_COEFF; j++)
    rop[j] = scalar * op[j];
}

//~ Computes pa(X)*pb(X) mod(X^8 - 2) and puts the result in "tmp_prod_result"
//(see file structs_data.h). ~ Note : E(X) = X^8 - 2 and nb_coeff(pa) =
// nb_coeff(pb) = 8.
void mult_mod_poly(int64_t* pa, int64_t* pb) {
  tmp_prod_result[0] = ((int128)pa[0] * pb[0]) +
                       ((((int128)pa[1] * pb[7]) + ((int128)pa[2] * pb[6]) +
                         ((int128)pa[3] * pb[5]) + ((int128)pa[4] * pb[4]) +
                         ((int128)pa[5] * pb[3]) + ((int128)pa[6] * pb[2]) +
                         ((int128)pa[7] * pb[1]))
                        << 1);

  tmp_prod_result[1] = ((int128)pa[0] * pb[1]) + ((int128)pa[1] * pb[0]) +
                       ((((int128)pa[2] * pb[7]) + ((int128)pa[3] * pb[6]) +
                         ((int128)pa[4] * pb[5]) + ((int128)pa[5] * pb[4]) +
                         ((int128)pa[6] * pb[3]) + ((int128)pa[7] * pb[2]))
                        << 1);

  tmp_prod_result[2] = ((int128)pa[0] * pb[2]) + ((int128)pa[1] * pb[1]) +
                       ((int128)pa[2] * pb[0]) +
                       ((((int128)pa[3] * pb[7]) + ((int128)pa[4] * pb[6]) +
                         ((int128)pa[5] * pb[5]) + ((int128)pa[6] * pb[4]) +
                         ((int128)pa[7] * pb[3]))
                        << 1);

  tmp_prod_result[3] = ((int128)pa[0] * pb[3]) + ((int128)pa[1] * pb[2]) +
                       ((int128)pa[2] * pb[1]) + ((int128)pa[3] * pb[0]) +
                       ((((int128)pa[4] * pb[7]) + ((int128)pa[5] * pb[6]) +
                         ((int128)pa[6] * pb[5]) + ((int128)pa[7] * pb[4]))
                        << 1);

  tmp_prod_result[4] = ((int128)pa[0] * pb[4]) + ((int128)pa[1] * pb[3]) +
                       ((int128)pa[2] * pb[2]) + ((int128)pa[3] * pb[1]) +
                       ((int128)pa[4] * pb[0]) +
                       ((((int128)pa[5] * pb[7]) + ((int128)pa[6] * pb[6]) +
                         ((int128)pa[7] * pb[5]))
                        << 1);

  tmp_prod_result[5] =
      ((int128)pa[0] * pb[5]) + ((int128)pa[1] * pb[4]) +
      ((int128)pa[2] * pb[3]) + ((int128)pa[3] * pb[2]) +
      ((int128)pa[4] * pb[1]) + ((int128)pa[5] * pb[0]) +
      ((((int128)pa[6] * pb[7]) + ((int128)pa[7] * pb[6])) << 1);

  tmp_prod_result[6] = ((int128)pa[0] * pb[6]) + ((int128)pa[1] * pb[5]) +
                       ((int128)pa[2] * pb[4]) + ((int128)pa[3] * pb[3]) +
                       ((int128)pa[4] * pb[2]) + ((int128)pa[5] * pb[1]) +
                       ((int128)pa[6] * pb[0]) + (((int128)pa[7] * pb[7]) << 1);

  tmp_prod_result[7] = ((int128)pa[0] * pb[7]) + ((int128)pa[1] * pb[6]) +
                       ((int128)pa[2] * pb[5]) + ((int128)pa[3] * pb[4]) +
                       ((int128)pa[4] * pb[3]) + ((int128)pa[5] * pb[2]) +
                       ((int128)pa[6] * pb[1]) + ((int128)pa[7] * pb[0]);
}

//~ Computes pa(X)*pb(X) mod(X^8 - 2) and puts the result in "tmp_prod_result_2"
//(see file structs_data.h). ~ Note : E(X) = X^8 - 2 and nb_coeff(pa) =
// nb_coeff(pb) = 8. TODO
void mult_mod_poly_2(int64_t* pa, int64_t* pb) {
  tmp_prod_result_2[0] = add_128(
      mul_64(pa[0], pb[0]),
      SHIFT_1(S_DOUBLE)(add_128(
          mul_64(pa[1], pb[7]),
          add_128(mul_64(pa[2], pb[6]),
                  add_128(mul_64(pa[3], pb[5]),
                          add_128(mul_64(pa[4], pb[4]),
                                  add_128(mul_64(pa[5], pb[3]),
                                          add_128(mul_64(pa[6], pb[2]),
                                                  mul_64(pa[7], pb[1])))))))));

  tmp_prod_result_2[1] = add_128(
      mul_64(pa[0], pb[1]),
      add_128(mul_64(pa[1], pb[0]),
              SHIFT_1(S_DOUBLE)(add_128(
                  mul_64(pa[2], pb[7]),
                  add_128(mul_64(pa[3], pb[6]),
                          add_128(mul_64(pa[4], pb[5]),
                                  add_128(mul_64(pa[5], pb[4]),
                                          add_128(mul_64(pa[6], pb[3]),
                                                  mul_64(pa[7], pb[2])))))))));

  tmp_prod_result_2[2] = add_128(
      mul_64(pa[0], pb[2]),
      add_128(mul_64(pa[1], pb[1]),
              add_128(mul_64(pa[2], pb[0]),
                      SHIFT_1(S_DOUBLE)(add_128(
                          mul_64(pa[3], pb[7]),
                          add_128(mul_64(pa[4], pb[6]),
                                  add_128(mul_64(pa[5], pb[5]),
                                          add_128(mul_64(pa[6], pb[4]),
                                                  mul_64(pa[7], pb[3])))))))));

  tmp_prod_result_2[3] = add_128(
      mul_64(pa[0], pb[3]),
      add_128(mul_64(pa[1], pb[2]),
              add_128(mul_64(pa[2], pb[1]),
                      add_128(mul_64(pa[3], pb[0]),
                              SHIFT_1(S_DOUBLE)(add_128(
                                  mul_64(pa[4], pb[7]),
                                  add_128(mul_64(pa[5], pb[6]),
                                          add_128(mul_64(pa[6], pb[5]),
                                                  mul_64(pa[7], pb[4])))))))));

  tmp_prod_result_2[4] = add_128(
      mul_64(pa[0], pb[4]),
      add_128(mul_64(pa[1], pb[3]),
              add_128(mul_64(pa[2], pb[2]),
                      add_128(mul_64(pa[3], pb[1]),
                              add_128(mul_64(pa[4], pb[0]),
                                      SHIFT_1(S_DOUBLE)(add_128(
                                          mul_64(pa[5], pb[7]),
                                          add_128(mul_64(pa[6], pb[6]),
                                                  mul_64(pa[7], pb[5])))))))));

  tmp_prod_result_2[5] = add_128(
      mul_64(pa[0], pb[5]),
      add_128(mul_64(pa[1], pb[4]),
              add_128(mul_64(pa[2], pb[3]),
                      add_128(mul_64(pa[3], pb[2]),
                              add_128(mul_64(pa[4], pb[1]),
                                      add_128(mul_64(pa[5], pb[0]),
                                              SHIFT_1(S_DOUBLE)(add_128(
                                                  mul_64(pa[6], pb[7]),
                                                  mul_64(pa[7], pb[6])))))))));

  tmp_prod_result_2[6] = add_128(
      mul_64(pa[0], pb[6]),
      add_128(mul_64(pa[1], pb[5]),
              add_128(mul_64(pa[2], pb[4]),
                      add_128(mul_64(pa[3], pb[3]),
                              add_128(mul_64(pa[4], pb[2]),
                                      add_128(mul_64(pa[5], pb[1]),
                                              add_128(mul_64(pa[6], pb[0]),
                                                      SHIFT_1(S_DOUBLE)(mul_64(
                                                          pa[7], pb[7])))))))));

  tmp_prod_result_2[7] = add_128(
      mul_64(pa[0], pb[7]),
      add_128(
          mul_64(pa[1], pb[6]),
          add_128(mul_64(pa[2], pb[5]),
                  add_128(mul_64(pa[3], pb[4]),
                          add_128(mul_64(pa[4], pb[3]),
                                  add_128(mul_64(pa[5], pb[2]),
                                          add_128(mul_64(pa[6], pb[1]),
                                                  mul_64(pa[7], pb[0]))))))));
}

//~ Assumes allocation already done for "rop".
void from_int_to_amns(int64_t* rop, mpz_t op) {
  int i, j;
  int64_t tmp_poly[NB_COEFF];

  for (i = 0; i < NB_COEFF; i++) {
    rop[i] = 0;
  }

  if (op->_mp_size == 0)
    return;

  rop[0] = (uint32_t)(op->_mp_d[0]);
  scalar_mult_poly(tmp_poly, CONV_P[0], (uint32_t)(op->_mp_d[0] >> 32));
  add_poly(rop, rop, tmp_poly);
  j = 1;
  for (i = 1; i < abs(op->_mp_size); i++) {
    scalar_mult_poly(tmp_poly, CONV_P[j++], (uint32_t)(op->_mp_d[i]));
    add_poly(rop, rop, tmp_poly);
    scalar_mult_poly(tmp_poly, CONV_P[j++], (uint32_t)(op->_mp_d[i] >> 32));
    add_poly(rop, rop, tmp_poly);
  }

  if (op->_mp_size < 0)
    neg_poly(rop, rop);

  internal_reduction_1(rop, rop);
}

//~ Assumes "rop" already initialized.
void from_amns_to_int(mpz_t rop, int64_t* op) {
  int i;
  mpz_t tmp;
  mpz_init(tmp);

  // printf("On rentre ds la conversion\n");

  mpz_set_si(rop, op[0]);
  for (i = 0; i < POLY_DEG; i++) {
    mpz_mul_si(tmp, gama_pow[i], op[i + 1]);
    mpz_add(rop, rop, tmp);
  }
  mpz_mod(rop, rop, modul_p);

  mpz_clear(tmp);
}

//~ performs the internal reduction on "op" and puts the result in "rop"
void internal_reduction_1(int64_t* rop, int64_t* op) {
  int i;
  for (i = 0; i < NB_COEFF; i++) {
    tmp_prod_result[i] = op[i];
  }
  internal_reduction_2(rop);
}

//~ performs the internal reduction on "op" and puts the result in "rop" TODO
void internal_reduction_1_2(int64_t* rop, int64_t* op) {
  int i;
  for (i = 0; i < NB_COEFF; i++) {
    tmp_prod_result_2[i].low = op[i];
    tmp_prod_result_2[i].high = 0;
  }
  internal_reduction_2_2(rop);
}

//~ Note : assumes 'num' > 0
int log2_uint128(uint128 num) {
  if ((num >> 64) != 0)
    return (127 - __builtin_clzl((uint64_t)(num >> 64)));
  else
    return (63 - __builtin_clzl((uint64_t)num));
}

//~ Returns log2(inf_norm('tmp_prod_result')),
//~ Also, updates 'tmp_sign_coeff_poly' (and 'tmp_prod_result')  according the
// signs of 'tmp_prod_result' coeffs. ~ Note : we pose log2(0) = 0.
int find_max_log2_coeff() {
  int i, tmp_log, max_log;

  max_log = 0;
  for (i = 0; i < NB_COEFF; i++) {
    if (tmp_prod_result[i] > 0) {
      tmp_sign_coeff_poly[i] = 1;
    } else if (tmp_prod_result[i] < 0) {
      tmp_sign_coeff_poly[i] = -1;
      tmp_prod_result[i] = -tmp_prod_result[i];
    } else {
      tmp_sign_coeff_poly[i] = 0;
      continue;
    }

    tmp_log = log2_uint128(tmp_prod_result[i]);
    if (max_log < tmp_log)
      max_log = tmp_log;
  }
  return max_log;
}

//~ Returns log2(inf_norm('tmp_prod_result')), TODO
//~ Also, updates 'tmp_sign_coeff_poly' (and 'tmp_prod_result_2')  according the
// signs of 'tmp_prod_result_2' coeffs. ~ Note : we pose log2(0) = 0.
int find_max_log2_coeff_2() {
  int i, tmp_log, max_log;

  max_log = 0;
  for (i = 0; i < NB_COEFF; i++) {
    if (tmp_prod_result_2[i].high < 0) {
      tmp_sign_coeff_poly[i] = -1;
      tmp_prod_result_2[i] = neg_128(tmp_prod_result_2[i]);
    } else if (tmp_prod_result_2[i].high | tmp_prod_result_2[i].low) {
      tmp_sign_coeff_poly[i] = 1;
    } else {
      tmp_sign_coeff_poly[i] = 0;
      continue;
    }

    tmp_log = LOG2(S_DOUBLE)(tmp_prod_result_2[i]);
    if (max_log < tmp_log)
      max_log = tmp_log;
  }
  return max_log;
}

//~ performs the complete internal reduction on "tmp_prod_result"
//~ and puts the result in "rop"
void internal_reduction_2(int64_t* rop) {
  int i, l, llog;
  int128 mask;

  llog = find_max_log2_coeff();
  // printf("llog %d %d\n",llog, COEFF_SIZE);
  while (llog >= COEFF_SIZE) {
    llog++;
    l = (llog > RED_KE) ? (llog - RED_KE) : 0;
    mask = 1;
    mask <<= l;
    mask--;

    for (i = 0; i < NB_COEFF; i++) {
      tmp_red_coeff_poly_high[i] = (int64_t)(tmp_prod_result[i] >> l);

      tmp_red_coeff_poly_low[i] =
          (int128)(tmp_prod_result[i] & mask) * tmp_sign_coeff_poly[i];
    }

    red_int(tmp_red_coeff_poly_high);

    for (i = 0; i < NB_COEFF; i++) {
      tmp_prod_result[i] =
          ((int128)tmp_red_coeff_poly_high[i] << l) + tmp_red_coeff_poly_low[i];
    }

    llog = find_max_log2_coeff();
  }

  for (i = 0; i < NB_COEFF; i++) {
    rop[i] = tmp_prod_result[i] * tmp_sign_coeff_poly[i];
  }
}

//~ performs the complete internal reduction on "tmp_prod_result_2" TODO
//~ and puts the result in "rop"
void internal_reduction_2_2(int64_t* rop) {
  int i, l, llog;

  llog = find_max_log2_coeff_2();
  // printf("llog %d %d\n",llog, COEFF_SIZE);
  while (llog >= COEFF_SIZE) {
    llog++;
    l = (llog > RED_KE) ? (llog - RED_KE) : 0;

    for (i = 0; i < NB_COEFF; i++) {
      tmp_red_coeff_poly_high[i] =
          SHIFT_R(S_DOUBLE)(tmp_prod_result_2[i], l).low;

      tmp_red_coeff_poly_low_2[i] =
          tmp_sign_coeff_poly[i]
              ? (tmp_sign_coeff_poly[i] == 1
                     ? MASK(S_DOUBLE)(tmp_prod_result_2[i], C_MASK(S_DOUBLE)(l))
                     : NEG(S_DOUBLE)(MASK(S_DOUBLE)(tmp_prod_result_2[i],
                                                    C_MASK(S_DOUBLE)(l))))
              : ZERO(S_DOUBLE)();
    }

    red_int(tmp_red_coeff_poly_high);

    for (i = 0; i < NB_COEFF; i++) {
      tmp_prod_result_2[i] =
          ADD(128)(from_int((int128)tmp_red_coeff_poly_high[i] << l),
                   tmp_red_coeff_poly_low_2[i]);
    }

    llog = find_max_log2_coeff_2();
  }

  for (i = 0; i < NB_COEFF; i++) {
    rop[i] = to_int(tmp_prod_result_2[i]) * tmp_sign_coeff_poly[i];
  }
}

//~ performs intermediate internal reduction on "op"
//~ the result is put in "op"
void red_int(int64_t* op) {
  int i;

  for (i = 0; i < NB_COEFF; i++) {
    tmp_red_int_poly_high[i] =
        (int64_t)(op[i] >> K_VAL) * tmp_sign_coeff_poly[i];

    tmp_red_int_poly_low[i] =
        (int64_t)(op[i] & red_int_mask) * tmp_sign_coeff_poly[i];
  }

  //~ corresponds to "tmp_red_int_poly_high * red_matrix"
  op[0] = tmp_red_int_poly_high[0] + (tmp_red_int_poly_high[3] << 1);
  op[1] = tmp_red_int_poly_high[1] + (tmp_red_int_poly_high[4] << 1);
  op[2] = tmp_red_int_poly_high[2] + (tmp_red_int_poly_high[5] << 1);
  op[3] = tmp_red_int_poly_high[3] + (tmp_red_int_poly_high[6] << 1);
  op[4] = tmp_red_int_poly_high[4] + (tmp_red_int_poly_high[7] << 1);
  op[5] = tmp_red_int_poly_high[5] + tmp_red_int_poly_high[0];
  op[6] = tmp_red_int_poly_high[6] + tmp_red_int_poly_high[1];
  op[7] = tmp_red_int_poly_high[7] + tmp_red_int_poly_high[2];

  for (i = 0; i < NB_COEFF; i++) {
    op[i] += tmp_red_int_poly_low[i];
  }
}

void init_datas() {
  int i;

  mpz_init(modul_p);
  for (i = 0; i < POLY_DEG; i++)
    mpz_init(gama_pow[i]);

  mpz_set_str(modul_p,
              "11579208902163662226212471516033475687780424538698063302004103"
              "5952359812890593",
              10);
  mpz_set_str(gama_pow[0],
              "14474011127704577782765589395224532314179217058921488395049827"
              "733759590399996",
              10);
  for (i = 1; i < POLY_DEG; i++) {
    mpz_mul(gama_pow[i], gama_pow[i - 1], gama_pow[0]);
    mpz_mod(gama_pow[i], gama_pow[i], modul_p);
  }
  red_int_mask = (1UL << K_VAL) - 1;
  // int64_t CONV_P[POLY_DEG][NB_COEFF] = {    /// C'est fucking local
  // 				{1, 0, 0, 0, 0, 1, 0, 0},
  // 				{1, 0, 2, 0, 0, 2, 0, 0},
  // 				{1, 0, 6, 0, 0, 3, 0, 2},
  // 				{1, 0, 12, 0, 4, 4, 0, 8},
  // 				{1, 8, 20, 0, 20, 5, 0, 20},
  // 				{1, 48, 30, 0, 60, 6, 8, 40},
  // 				{1, 168, 42, 16, 140, 7, 56, 70} };
}

void free_datas() {
  int i;

  mpz_clear(modul_p);
  for (i = 0; i < POLY_DEG; i++)
    mpz_clear(gama_pow[i]);
}

void check_convertion(mpz_t A) {}

void check_add(mpz_t A, mpz_t B) {}

void check_mult(mpz_t A, mpz_t B) {}

void bench(int nbiter) {
  //	printf("Nb iter : %d\n", nbiter);
}
