#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 8380803616454170272UL, .high = -613162L}, {.low = 16592186725946204235UL, .high = 315871L}, {.low = 7803718093850047851UL, .high = -533618L}};
int128_t poly_P1[NB_COEFF] = {{.low = 12883975821583647669UL, .high = -2312396L}, {.low = 16037060097019842026UL, .high = 388507L}, {.low = 10735652819890782938UL, .high = -437763L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 5268733878218200539UL, .high = 1678491L}, {.low = 5919513906426651535UL, .high = 892866L}, {.low = 5308286956862964915UL, .high = 1160409L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 3322915119783866847UL, .high = 2869446998571952945UL}, {.low = 4061378594672334631UL, .high = 15029430525086962233UL}, {.low = 12386489254630410498UL, .high = 7027791858440168631UL}};

