#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 3804046392669885977UL, .high = 4946183902243899L}, {.low = 5798806588029387888UL, .high = 5497996814418303L}, {.low = 8988491859855142063UL, .high = -554363208875116L}};
int128_t poly_P1[NB_COEFF] = {{.low = 18017532591052753605UL, .high = -1897794285614282L}, {.low = 16228180255723651723UL, .high = 5236529234246951L}, {.low = 15573271068861186700UL, .high = 3282985033926358L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 8421154893985060511UL, .high = 7110078929953360L}, {.low = 14778977013953854075UL, .high = 1834871833348917L}, {.low = 3406462683292352196UL, .high = 87407128465355L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 14632730325508246903UL, .high = 12121498165549095546UL}, {.low = 8254057750114659291UL, .high = 1532440348617858786UL}, {.low = 9199816693508626315UL, .high = 2076861966115420789UL}};

