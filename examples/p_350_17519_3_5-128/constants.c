#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 11931774876989978374UL, .high = 21556399294077032L}, {.low = 14555097011809589967UL, .high = 1279789328256665L}, {.low = 16758457417611838833UL, .high = 1913914932018907L}};
int128_t poly_P1[NB_COEFF] = {{.low = 16555946837297536564UL, .high = 18009679065484286L}, {.low = 17411280345583166159UL, .high = 9250647357661710L}, {.low = 2676392105931845922UL, .high = 1026562847937342L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 5694724384469991955UL, .high = 3539341587453934L}, {.low = 15793597763075474864UL, .high = 4815560412447811L}, {.low = 3199186205878662506UL, .high = 5375942368614616L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 16951834930403915277UL, .high = 11295584904366980705UL}, {.low = 4124837771719729684UL, .high = 3050689226690023083UL}, {.low = 8658159925765439210UL, .high = 9658877432352733519UL}};

