#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 2582435365341061799UL, .high = 13799516100234970L}, {.low = 3020002892005189185UL, .high = 12546543141334610L}, {.low = 17729290333421982392UL, .high = 9230301080460195L}};
int128_t poly_P1[NB_COEFF] = {{.low = 8585232856578263134UL, .high = 11319772318745463L}, {.low = 14538499517523739682UL, .high = 9971566029572504L}, {.low = 6409585468702890777UL, .high = 5954150402576355L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 1929090443353932699UL, .high = 7294501059871231L}, {.low = 8387715048591392206UL, .high = 1526467996094611L}, {.low = 5758519670497527114UL, .high = 1816217649397767L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 2312923892322871621UL, .high = 18282729698819934986UL}, {.low = 14680393972521069842UL, .high = 17477965602654271155UL}, {.low = 2935441600339420630UL, .high = 16116046071961569771UL}};

