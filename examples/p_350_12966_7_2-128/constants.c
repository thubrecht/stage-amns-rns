#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 18446506162912286903UL, .high = -1L}, {.low = 18445290014635064777UL, .high = -1L}, {.low = 18444989782002948474UL, .high = -1L}, {.low = 18446070146208481184UL, .high = -1L}, {.low = 89892149694770UL, .high = 0L}, {.low = 18446219384220598467UL, .high = -1L}, {.low = 18446470945891236543UL, .high = -1L}};
int128_t poly_P1[NB_COEFF] = {{.low = 18446486696658385638UL, .high = -1L}, {.low = 18446484314364569345UL, .high = -1L}, {.low = 18445365399355561088UL, .high = -1L}, {.low = 18446146023470907863UL, .high = -1L}, {.low = 18446709493522229726UL, .high = -1L}, {.low = 18445978406586071129UL, .high = -1L}, {.low = 18446174561205896101UL, .high = -1L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 84208135246081UL, .high = 0L}, {.low = 149871372388081UL, .high = 0L}, {.low = 310433837818858UL, .high = 0L}, {.low = 640689787689762UL, .high = 0L}, {.low = 454896606925813UL, .high = 0L}, {.low = 77877352829619UL, .high = 0L}, {.low = 41277970702185UL, .high = 0L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 357768133383612485UL, .high = 17442312097525709847UL}, {.low = 16987654278432812121UL, .high = 17259340346150557991UL}, {.low = 10716613826927880441UL, .high = 13679639900934642663UL}, {.low = 17019018118666227555UL, .high = 9554885217848322058UL}, {.low = 10937761349647265076UL, .high = 12293919327442159755UL}, {.low = 5043250521640442132UL, .high = 468621684486015098UL}, {.low = 2394200085789864407UL, .high = 15697319409626007159UL}};

