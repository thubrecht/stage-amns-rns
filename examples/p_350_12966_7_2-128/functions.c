#include "functions.h"

#include <gmp.h>
#include <stdio.h>

#include "../convert.h"
#include "../integers.h"

#include "operations.h"
#include "structs_data.h"

//~ Assumes allocation already done for 'rop'.
//~ IMPORTANT : convertion to montgomery domain will be done here
void from_int_to_amns(int128_t* rop, mpz_t op) {
  int i;
  mpz_t tmp;
  int128_t mask;
  int256_t tmp_poly[NB_COEFF];
  int256_t tmp_sum[NB_COEFF];

  mpz_init_set(tmp, op);

  for (int i = 0; i < NB_COEFF; i++) {
    rop[i] = zero_128();
    tmp_sum[i] = zero_256();
  }

  if (tmp->_mp_size == 0) {
    return;
  }

  mask = create_mask_128(RHO_LOG2);

  scalar_mult_lpoly(tmp_poly, poly_P0, mask_128(low_128(tmp), mask));
  add_lpoly(tmp_sum, tmp_sum, tmp_poly);

  mpz_tdiv_q_2exp(tmp, tmp, RHO_LOG2);

  if (tmp->_mp_size) {
    scalar_mult_lpoly(tmp_poly, poly_P1, mask_128(low_128(tmp), mask));
    add_lpoly(tmp_sum, tmp_sum, tmp_poly);

    mpz_tdiv_q_2exp(tmp, tmp, RHO_LOG2);
  }

  i = 0;
  while (tmp->_mp_size) {
    scalar_mult_lpoly(tmp_poly, polys_P[i++], mask_128(low_128(tmp), mask));
    add_lpoly(tmp_sum, tmp_sum, tmp_poly);

    mpz_tdiv_q_2exp(tmp, tmp, RHO_LOG2);
  }

  internal_reduction(rop, tmp_sum);

  mpz_clear(tmp);
}

//~ Assumes "rop" already initialized.
//~ IMPORTANT : convertion from montgomery domain will be done here.
void from_amns_to_int(mpz_t rop, int128_t* op) {
  mpz_t tmp_sum, tmp;
  int128_t tmp_conv[NB_COEFF];

  mpz_inits(tmp_sum, tmp, NULL);

  //~ convertion out of mont domain
  from_mont_domain(tmp_conv, op);

  mpz_from_128(rop, tmp_conv[0]);
  for (int i = 0; i < POLY_DEG; i++) {
    mpz_from_128(tmp, tmp_conv[i + 1]);
    mpz_mul(tmp_sum, gama_pow[i], tmp);
    mpz_add(rop, rop, tmp_sum);
  }

  mpz_mod(rop, rop, modul_p);

  mpz_clear(tmp_sum);
}

//~ computes : op/phi
extern inline void from_mont_domain(int128_t* rop, int128_t* op);

//~ return a positive value if pa > pb, zero if pa = pb, or a negative value if pa < pb.
//~ Important : evaluation is done using the corresponding integers modulo 'p'.
int cmp_poly_evals(int128_t* pa, int128_t* pb) {
  int rep;
  mpz_t a, b;
  mpz_inits(a, b, NULL);
  from_amns_to_int(a, pa);
  from_amns_to_int(b, pb);
  rep = mpz_cmp(a, b);
  mpz_clears(a, b, NULL);
  return rep;
}

void copy_poly(int128_t* rop, int128_t* op) {
  int i;
  for (i = 0; i < NB_COEFF; i++)
    rop[i] = op[i];
}

void add_lpoly(int256_t* rop, int256_t* pa, int256_t* pb) {
  for (int j = 0; j < NB_COEFF; j++) {
    rop[j] = add_256(pa[j], pb[j]);
  }
}

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
extern inline void scalar_mult_lpoly(int256_t* rop, int128_t* op, int128_t scalar);

void print_element(int128_t* poly) {
  int i;
  printf("[");
  for (i = 0; i < POLY_DEG; i++)
    printf("%2ld, ", poly[i]);
  printf("%2ld]", poly[POLY_DEG]);
}
