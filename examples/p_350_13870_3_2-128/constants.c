#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 5378359747964314514UL, .high = 736953691514178L}, {.low = 16068977224763656935UL, .high = 5548850839225411L}, {.low = 17520689706161590799UL, .high = 4044758307359613L}};
int128_t poly_P1[NB_COEFF] = {{.low = 13204403293375078027UL, .high = 2855598519853941L}, {.low = 10549601176257392686UL, .high = 5954282088812893L}, {.low = 14136649848873740621UL, .high = 3639779545291018L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 1204529023360890677UL, .high = 1411742047857427L}, {.low = 6164559788858387731UL, .high = 474480816530537L}, {.low = 358992788330181252UL, .high = 3705236328034460L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 4763695724695241201UL, .high = 14346965584958588926UL}, {.low = 7993612416511485103UL, .high = 13005233252486337379UL}, {.low = 16445291048300987637UL, .high = 15007748399875066251UL}};

