#include "../integers.h"

#include "structs_data.h"

#include "operations.h"


void add_poly(int64_t *rop, int64_t *pa, int64_t *pb){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = add_64(pa[j], pb[j]);
}

void sub_poly(int64_t *rop, int64_t *pa, int64_t *pb){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = sub_64(pa[j], pb[j]);
}

void neg_poly(int64_t *rop, int64_t *op){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = neg_64(op[j]);
}

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
void scalar_mult_poly(int64_t *rop, int64_t *op, int64_t scalar){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = scal_64(scalar, op[j]);
}

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
void double_poly_coeffs(int64_t *rop, int64_t *op){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = shift_by_1_64(op[j]);
}

//~ assumes 'nb_pos' and/or coeffs of 'op' small enough to avoid an overflow.
void lshift_poly_coeffs(int64_t *rop, int64_t *op, int nb_pos){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = shift_l_64(op[j], nb_pos);
}

//~ Computes pa(X)*pb(X) mod(E)
void mult_mod_poly(int64_t *rop, int64_t *pa, int64_t *pb){

    int128_t tmp_prod_result[NB_COEFF];
    tmp_prod_result[0] = add_128(mul_64(pa[0], pb[0]), shift_by_1_128(add_128(add_128(add_128(mul_64(pa[1], pb[4]), mul_64(pa[2], pb[3])), mul_64(pa[3], pb[2])), mul_64(pa[4], pb[1]))));
    tmp_prod_result[1] = add_128(add_128(mul_64(pa[0], pb[1]), mul_64(pa[1], pb[0])), shift_by_1_128(add_128(add_128(mul_64(pa[2], pb[4]), mul_64(pa[3], pb[3])), mul_64(pa[4], pb[2]))));
    tmp_prod_result[2] = add_128(add_128(add_128(mul_64(pa[0], pb[2]), mul_64(pa[1], pb[1])), mul_64(pa[2], pb[0])), shift_by_1_128(add_128(mul_64(pa[3], pb[4]), mul_64(pa[4], pb[3]))));
    tmp_prod_result[3] = add_128(add_128(add_128(add_128(mul_64(pa[0], pb[3]), mul_64(pa[1], pb[2])), mul_64(pa[2], pb[1])), mul_64(pa[3], pb[0])), shift_by_1_128(mul_64(pa[4], pb[4])));
    tmp_prod_result[4] = add_128(add_128(add_128(add_128(mul_64(pa[0], pb[4]), mul_64(pa[1], pb[3])), mul_64(pa[2], pb[2])), mul_64(pa[3], pb[1])), mul_64(pa[4], pb[0]));

    internal_reduction(rop, tmp_prod_result);
}

//~ Computes pa(X)^2 mod(E)
void square_mod_poly(int64_t *rop, int64_t *pa){

    int128_t tmp_prod_result[NB_COEFF];
    tmp_prod_result[0] = add_128(mul_64(pa[0], pa[0]), shift_l_128(add_128(mul_64(pa[3], pa[2]), mul_64(pa[4], pa[1])), 2));
    tmp_prod_result[1] = add_128(shift_by_1_128(mul_64(pa[1], pa[0])), shift_by_1_128(add_128(shift_by_1_128(mul_64(pa[4], pa[2])), mul_64(pa[3], pa[3]))));
    tmp_prod_result[2] = add_128(add_128(shift_by_1_128(mul_64(pa[2], pa[0])), mul_64(pa[1], pa[1])), shift_l_128(mul_64(pa[4], pa[3]), 2));
    tmp_prod_result[3] = add_128(shift_by_1_128(add_128(mul_64(pa[2], pa[1]), mul_64(pa[3], pa[0]))), shift_by_1_128(mul_64(pa[4], pa[4])));
    tmp_prod_result[4] = add_128(shift_by_1_128(add_128(mul_64(pa[3], pa[1]), mul_64(pa[4], pa[0]))), mul_64(pa[2], pa[2]));

    internal_reduction(rop, tmp_prod_result);
}

//~ performs the internal reduction on 'op' and puts the result in 'rop'
//~ IMPORTANT : We take 'mont_phi = 1 << WORD_SIZE', so operations modulo mont_phi are automatically done using the appropriate variable type.
extern inline void internal_reduction(int64_t *rop, int128_t *op);

void exact_coeffs_reduction(int64_t *rop, int64_t *op){

    int i;
    int128_t tmp[NB_COEFF];

    for(i=0; i<NB_COEFF; i++)
        tmp[i] = grow_64(op[i]);

    internal_reduction(rop, tmp);

    tmp[0] = add_128(mul_64(rop[0], poly_P0[0]), shift_by_1_128(add_128(add_128(add_128(mul_64(rop[1], poly_P0[4]), mul_64(rop[2], poly_P0[3])), mul_64(rop[3], poly_P0[2])), mul_64(rop[4], poly_P0[1]))));
    tmp[1] = add_128(add_128(mul_64(rop[0], poly_P0[1]), mul_64(rop[1], poly_P0[0])), shift_by_1_128(add_128(add_128(mul_64(rop[2], poly_P0[4]), mul_64(rop[3], poly_P0[3])), mul_64(rop[4], poly_P0[2]))));
    tmp[2] = add_128(add_128(add_128(mul_64(rop[0], poly_P0[2]), mul_64(rop[1], poly_P0[1])), mul_64(rop[2], poly_P0[0])), shift_by_1_128(add_128(mul_64(rop[3], poly_P0[4]), mul_64(rop[4], poly_P0[3]))));
    tmp[3] = add_128(add_128(add_128(add_128(mul_64(rop[0], poly_P0[3]), mul_64(rop[1], poly_P0[2])), mul_64(rop[2], poly_P0[1])), mul_64(rop[3], poly_P0[0])), shift_by_1_128(mul_64(rop[4], poly_P0[4])));
    tmp[4] = add_128(add_128(add_128(add_128(mul_64(rop[0], poly_P0[4]), mul_64(rop[1], poly_P0[3])), mul_64(rop[2], poly_P0[2])), mul_64(rop[3], poly_P0[1])), mul_64(rop[4], poly_P0[0]));

    internal_reduction(rop, tmp);
}

