#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 5588059239057908785UL, .high = -106801L}, {.low = 11584537796012966742UL, .high = -910294L}, {.low = 6762227544637531669UL, .high = -527470L}};
int128_t poly_P1[NB_COEFF] = {{.low = 13399779409438027893UL, .high = -2155433L}, {.low = 7115848162769446169UL, .high = -1395689L}, {.low = 11046552154854184045UL, .high = -1308445L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 11241284956674903921UL, .high = 47540L}, {.low = 8128867826260975026UL, .high = 373556L}, {.low = 17107689986548527381UL, .high = 1623982L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 18223210630740042373UL, .high = 8016058171088465462UL}, {.low = 15453060938997499844UL, .high = 5344786799358870224UL}, {.low = 3009668367190906063UL, .high = 816616580904236552UL}};

