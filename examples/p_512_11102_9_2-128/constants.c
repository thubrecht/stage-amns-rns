#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 96207322989125467UL, .high = 0L}, {.low = 121839591252014503UL, .high = 0L}, {.low = 188483990677128678UL, .high = 0L}, {.low = 91222633848455499UL, .high = 0L}, {.low = 18430826772916325552UL, .high = -1L}, {.low = 154189290998259347UL, .high = 0L}, {.low = 129891088160840398UL, .high = 0L}, {.low = 84756263138707418UL, .high = 0L}, {.low = 51748027242912573UL, .high = 0L}};
int128_t poly_P1[NB_COEFF] = {{.low = 128955522259534678UL, .high = 0L}, {.low = 51761430724794559UL, .high = 0L}, {.low = 134222410908945816UL, .high = 0L}, {.low = 106836867543376177UL, .high = 0L}, {.low = 44143974220358162UL, .high = 0L}, {.low = 73628334868660004UL, .high = 0L}, {.low = 123863962009782956UL, .high = 0L}, {.low = 87711507423738900UL, .high = 0L}, {.low = 29514492270324725UL, .high = 0L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 9327384319613939UL, .high = 0L}, {.low = 12753617388006938UL, .high = 0L}, {.low = 2115947739644824UL, .high = 0L}, {.low = 72072704700233792UL, .high = 0L}, {.low = 30829210841321152UL, .high = 0L}, {.low = 16726007275108571UL, .high = 0L}, {.low = 9214515023258381UL, .high = 0L}, {.low = 83558225898922454UL, .high = 0L}, {.low = 11506226900089032UL, .high = 0L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 354092454373174873UL, .high = 11941403757673517527UL}, {.low = 11758139851098107180UL, .high = 11835827362957294534UL}, {.low = 15290036364396265672UL, .high = 5488308114675930748UL}, {.low = 10745671789787975130UL, .high = 3667079932292411764UL}, {.low = 15699449902232270412UL, .high = 8851780516361037233UL}, {.low = 4748117315946231517UL, .high = 4016493323308880179UL}, {.low = 10135355798377303447UL, .high = 2153688074702953920UL}, {.low = 13740506628066430888UL, .high = 8786887408162408138UL}, {.low = 1217631065832733786UL, .high = 7646556070668461966UL}};

