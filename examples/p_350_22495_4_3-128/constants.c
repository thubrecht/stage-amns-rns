#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 18026129683006888460UL, .high = 4235643L}, {.low = 10518036748628426230UL, .high = -1390336L}, {.low = 5706769749770202599UL, .high = 6733223L}, {.low = 7891086627712230051UL, .high = 3751728L}};
int128_t poly_P1[NB_COEFF] = {{.low = 13762038474436731756UL, .high = 232928L}, {.low = 16262687848717830134UL, .high = 6241800L}, {.low = 16784367836905897637UL, .high = 5580976L}, {.low = 9135593133662436901UL, .high = -213484L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 4961603162441884110UL, .high = 5180900L}, {.low = 4268845593752615186UL, .high = 5496201L}, {.low = 15231769891263433532UL, .high = 2111581L}, {.low = 17414316893214147185UL, .high = 3807792L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 10916496129347905380UL, .high = 1912619512550131310UL}, {.low = 15752071765914862263UL, .high = 8050029359690840531UL}, {.low = 11405243538981705846UL, .high = 148814515176450583UL}, {.low = 13632761177326752886UL, .high = 9300446906334520818UL}};

