#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 12867663954515341328UL, .high = 3928348909784269L}, {.low = 11935045962939888583UL, .high = 4937629571933739L}, {.low = 4568415538650106011UL, .high = 4007913856511457L}};
int128_t poly_P1[NB_COEFF] = {{.low = 11056487002597745999UL, .high = 1579263438967731L}, {.low = 5665668194767932433UL, .high = 6659180792986379L}, {.low = 11655434066143779318UL, .high = 1533037025456646L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 2642047622345850641UL, .high = 1923197540133086L}, {.low = 8179064836096266672UL, .high = 1682957350550100L}, {.low = 7223075303912483901UL, .high = 3992972599919428L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 17525880712860513171UL, .high = 5066730233025053083UL}, {.low = 15580608526480249446UL, .high = 2353880822413226570UL}, {.low = 2017678588181053065UL, .high = 9994074622438779581UL}};

