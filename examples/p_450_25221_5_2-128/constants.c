#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 15811677164744004309UL, .high = -23931580L}, {.low = 14314832358106597695UL, .high = -86731715L}, {.low = 4446825030918463107UL, .high = -46031938L}, {.low = 5677898077377249355UL, .high = -70301603L}, {.low = 13088984946281739895UL, .high = -44835391L}};
int128_t poly_P1[NB_COEFF] = {{.low = 4938198758326013240UL, .high = -66844815L}, {.low = 10051320410850683558UL, .high = -66196783L}, {.low = 13917972803574270605UL, .high = -14186932L}, {.low = 13857422157554944973UL, .high = -34671242L}, {.low = 14249104866261928466UL, .high = -38559350L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 5645969658931912297UL, .high = 23464418L}, {.low = 3744808758039746440UL, .high = 33647908L}, {.low = 16491428272798894977UL, .high = 57076982L}, {.low = 15560721819054883668UL, .high = 4607501L}, {.low = 17353828033581740783UL, .high = 15329156L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 5237133410423045449UL, .high = 13234294686305493444UL}, {.low = 7283912340188487602UL, .high = 15282472305294988527UL}, {.low = 9294535373243080059UL, .high = 8833962842764364528UL}, {.low = 1115171934130911918UL, .high = 659099327509542770UL}, {.low = 14267708120429976822UL, .high = 6660071238065725102UL}};

