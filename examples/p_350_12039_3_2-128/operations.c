#include "../integers.h"

#include "structs_data.h"

#include "operations.h"


void add_poly(int128_t *rop, int128_t *pa, int128_t *pb){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = add_128(pa[j], pb[j]);
}

void sub_poly(int128_t *rop, int128_t *pa, int128_t *pb){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = sub_128(pa[j], pb[j]);
}

void neg_poly(int128_t *rop, int128_t *op){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = neg_128(op[j]);
}

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
void scalar_mult_poly(int128_t *rop, int128_t *op, int128_t scalar){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = scal_128(scalar, op[j]);
}

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
void double_poly_coeffs(int128_t *rop, int128_t *op){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = shift_by_1_128(op[j]);
}

//~ assumes 'nb_pos' and/or coeffs of 'op' small enough to avoid an overflow.
void lshift_poly_coeffs(int128_t *rop, int128_t *op, int nb_pos){
    int j;
    for (j=0; j<NB_COEFF; j++)
        rop[j] = shift_l_128(op[j], nb_pos);
}

//~ Computes pa(X)*pb(X) mod(E)
void mult_mod_poly(int128_t *rop, int128_t *pa, int128_t *pb){

    int256_t tmp_prod_result[NB_COEFF];
    tmp_prod_result[0] = add_256(mul_128(pa[0], pb[0]), shift_by_1_256(add_256(mul_128(pa[1], pb[2]), mul_128(pa[2], pb[1]))));
    tmp_prod_result[1] = add_256(add_256(mul_128(pa[0], pb[1]), mul_128(pa[1], pb[0])), shift_by_1_256(mul_128(pa[2], pb[2])));
    tmp_prod_result[2] = add_256(add_256(mul_128(pa[0], pb[2]), mul_128(pa[1], pb[1])), mul_128(pa[2], pb[0]));

    internal_reduction(rop, tmp_prod_result);
}

//~ Computes pa(X)^2 mod(E)
void square_mod_poly(int128_t *rop, int128_t *pa){

    int256_t tmp_prod_result[NB_COEFF];
    tmp_prod_result[0] = add_256(mul_128(pa[0], pa[0]), shift_l_256(mul_128(pa[2], pa[1]), 2));
    tmp_prod_result[1] = add_256(shift_by_1_256(mul_128(pa[1], pa[0])), shift_by_1_256(mul_128(pa[2], pa[2])));
    tmp_prod_result[2] = add_256(shift_by_1_256(mul_128(pa[2], pa[0])), mul_128(pa[1], pa[1]));

    internal_reduction(rop, tmp_prod_result);
}

//~ performs the internal reduction on 'op' and puts the result in 'rop'
//~ IMPORTANT : We take 'mont_phi = 1 << WORD_SIZE', so operations modulo mont_phi are automatically done using the appropriate variable type.
extern inline void internal_reduction(int128_t *rop, int256_t *op);

void exact_coeffs_reduction(int128_t *rop, int128_t *op){

    int i;
    int256_t tmp[NB_COEFF];

    for(i=0; i<NB_COEFF; i++)
        tmp[i] = grow_128(op[i]);

    internal_reduction(rop, tmp);

    tmp[0] = add_256(mul_128(rop[0], poly_P0[0]), shift_by_1_256(add_256(mul_128(rop[1], poly_P0[2]), mul_128(rop[2], poly_P0[1]))));
    tmp[1] = add_256(add_256(mul_128(rop[0], poly_P0[1]), mul_128(rop[1], poly_P0[0])), shift_by_1_256(mul_128(rop[2], poly_P0[2])));
    tmp[2] = add_256(add_256(mul_128(rop[0], poly_P0[2]), mul_128(rop[1], poly_P0[1])), mul_128(rop[2], poly_P0[0]));

    internal_reduction(rop, tmp);
}

