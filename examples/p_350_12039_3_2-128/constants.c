#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 3565523896801827444UL, .high = 5561085663998361L}, {.low = 7069605148166841123UL, .high = 684170434505255L}, {.low = 1286433704378436497UL, .high = 1330149868897092L}};
int128_t poly_P1[NB_COEFF] = {{.low = 4134604725120756667UL, .high = 3810152526334140L}, {.low = 16565499818517290963UL, .high = 3188532374811685L}, {.low = 12313361603838674515UL, .high = -1425428018993363L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 13028962224002781623UL, .high = 3118139757408025L}, {.low = 5650621459955676678UL, .high = 5325917287763264L}, {.low = 11745928638961036537UL, .high = 783873875435596L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 1623412538688938987UL, .high = 8856845519729253943UL}, {.low = 15212440958475672244UL, .high = 6762644168790410227UL}, {.low = 879456000151202947UL, .high = 247506447923621139UL}};

