#include "../integers.h"

#include "structs_data.h"

#ifndef POLY_MULT_ADD
#define POLY_MULT_ADD


void sub_poly(int128_t *rop, int128_t *pa, int128_t *pb);
void add_poly(int128_t *rop, int128_t *pa, int128_t *pb);
void neg_poly(int128_t *rop, int128_t *op);
void scalar_mult_poly(int128_t *rop, int128_t *op, int128_t scalar);
void double_poly_coeffs(int128_t *rop, int128_t *op);
void lshift_poly_coeffs(int128_t *rop, int128_t *op, int nb_pos);

void mult_mod_poly(int128_t *rop, int128_t *pa, int128_t *pb);

void square_mod_poly(int128_t *rop, int128_t *pa);

__always_inline void internal_reduction(int128_t *rop, int256_t *op) {
    uint128_t tmp_q[NB_COEFF];
    int256_t tmp_zero[NB_COEFF];

    //~ computation of: op*neg_inv_ri_rep_coeff mod((X^n - c), mont_phi)
    uint128_t q_coeff = {.low = 10119512918584588530UL, .high = 14882082429106347375UL};
    tmp_q[0] = addu_128(scalu_128(op[0].low, int_red_coeff_inv[0]), shift_by_1u_128(addu_128(addu_128(addu_128(scalu_128(op[1].low, int_red_coeff_inv[4]), scalu_128(op[2].low, int_red_coeff_inv[3])), scalu_128(op[3].low, int_red_coeff_inv[2])), scalu_128(op[4].low, int_red_coeff_inv[1]))));
    tmp_q[1] = addu_128(addu_128(scalu_128(op[0].low, int_red_coeff_inv[1]), scalu_128(op[1].low, int_red_coeff_inv[0])), shift_by_1u_128(addu_128(addu_128(scalu_128(op[2].low, int_red_coeff_inv[4]), scalu_128(op[3].low, int_red_coeff_inv[3])), scalu_128(op[4].low, int_red_coeff_inv[2]))));
    tmp_q[2] = addu_128(addu_128(addu_128(scalu_128(op[0].low, int_red_coeff_inv[2]), scalu_128(op[1].low, int_red_coeff_inv[1])), scalu_128(op[2].low, int_red_coeff_inv[0])), shift_by_1u_128(addu_128(scalu_128(op[3].low, int_red_coeff_inv[4]), scalu_128(op[4].low, int_red_coeff_inv[3]))));
    tmp_q[3] = addu_128(addu_128(addu_128(addu_128(scalu_128(op[0].low, int_red_coeff_inv[3]), scalu_128(op[1].low, int_red_coeff_inv[2])), scalu_128(op[2].low, int_red_coeff_inv[1])), scalu_128(op[3].low, int_red_coeff_inv[0])), scalu_128(op[4].low, q_coeff));
    tmp_q[4] = addu_128(addu_128(addu_128(addu_128(scalu_128(op[0].low, int_red_coeff_inv[4]), scalu_128(op[1].low, int_red_coeff_inv[3])), scalu_128(op[2].low, int_red_coeff_inv[2])), scalu_128(op[3].low, int_red_coeff_inv[1])), scalu_128(op[4].low, int_red_coeff_inv[0]));

    //~ computation of: tmp_q*red_int_coeff mod(X^n - c)
    int128_t z_coeff = {.low = 3611690300535187360UL, .high = 22026875288UL};

    tmp_zero[0] = add_256(neg_256(mul_128((int128_t)tmp_q[0], int_red_coeff_0[0])), shift_by_1_256(add_256(add_256(add_256(mul_128((int128_t)tmp_q[1], int_red_coeff_0[4]), neg_256(mul_128((int128_t)tmp_q[2], int_red_coeff_0[3]))), mul_128((int128_t)tmp_q[3], int_red_coeff_0[2])), mul_128((int128_t)tmp_q[4], int_red_coeff_0[1]))));
    tmp_zero[1] = add_256(add_256(mul_128((int128_t)tmp_q[0], int_red_coeff_0[1]), neg_256(mul_128((int128_t)tmp_q[1], int_red_coeff_0[0]))), shift_by_1_256(add_256(add_256(mul_128((int128_t)tmp_q[2], int_red_coeff_0[4]), neg_256(mul_128((int128_t)tmp_q[3], int_red_coeff_0[3]))), mul_128((int128_t)tmp_q[4], int_red_coeff_0[2]))));
    tmp_zero[2] = add_256(add_256(add_256(mul_128((int128_t)tmp_q[0], int_red_coeff_0[2]), mul_128((int128_t)tmp_q[1], int_red_coeff_0[1])), neg_256(mul_128((int128_t)tmp_q[2], int_red_coeff_0[0]))), shift_by_1_256(add_256(mul_128((int128_t)tmp_q[3], int_red_coeff_0[4]), neg_256(mul_128((int128_t)tmp_q[4], int_red_coeff_0[3])))));
    tmp_zero[3] = add_256(add_256(add_256(add_256(neg_256(mul_128((int128_t)tmp_q[0], int_red_coeff_0[3])), mul_128((int128_t)tmp_q[1], int_red_coeff_0[2])), mul_128((int128_t)tmp_q[2], int_red_coeff_0[1])), neg_256(mul_128((int128_t)tmp_q[3], int_red_coeff_0[0]))), mul_128((int128_t)tmp_q[4], z_coeff));
    tmp_zero[4] = add_256(add_256(add_256(add_256(mul_128((int128_t)tmp_q[0], int_red_coeff_0[4]), neg_256(mul_128((int128_t)tmp_q[1], int_red_coeff_0[3]))), mul_128((int128_t)tmp_q[2], int_red_coeff_0[2])), mul_128((int128_t)tmp_q[3], int_red_coeff_0[1])), neg_256(mul_128((int128_t)tmp_q[4], int_red_coeff_0[0])));

    //~ computation of: (op + tmp_zero)/mont_phi
    rop[0] = add_256(op[0], tmp_zero[0]).high;
    rop[1] = add_256(op[1], tmp_zero[1]).high;
    rop[2] = add_256(op[2], tmp_zero[2]).high;
    rop[3] = add_256(op[3], tmp_zero[3]).high;
    rop[4] = add_256(op[4], tmp_zero[4]).high;
}

void exact_coeffs_reduction(int128_t *rop, int128_t *op);

#endif

