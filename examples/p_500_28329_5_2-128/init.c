#include "../integers.h"

#include "structs_data.h"
#include "operations.h"
#include "functions.h"

#include <gmp.h>

#include "init.h"


int128_t polys_P[(NB_COEFF - 2)][NB_COEFF];

mpz_t modul_p;
mpz_t gama_pow[POLY_DEG];

void init_data(){
    int i;
    for(i=0; i<POLY_DEG; i++)
        mpz_init (gama_pow[i]);

    mpz_init (modul_p);


    mpz_set_str (modul_p, "2832914880249026501771102943967172211406715937901848044418164662208666905405940194153135923710717021244920694498775002434843586411439315565211005726303", 10);

    mpz_set_str (gama_pow[0], "1617642037052121576903610164439490082987215772219973480007550614589246357220545492310477611967103380963709865393444827622198000694140445962590797567355", 10);
    for(i=1; i<POLY_DEG; i++){
        mpz_mul (gama_pow[i], gama_pow[i-1], gama_pow[0]);
        mpz_mod (gama_pow[i], gama_pow[i], modul_p);
    }

    //~ IMPORTANT : initialisations above must be done before those below.
    compute_polys_P();
}


//~ computes representations of the polynomials P, used for conversion into the AMNS
void compute_polys_P(){
    int i, l;
    int128_t tmp_poly[NB_COEFF];

    //~ computation of a representation of 'phi*rho'
    from_mont_domain(tmp_poly, poly_P1);

    l = NB_COEFF - 2;
    if (l > 0){
        mult_mod_poly(polys_P[0], poly_P1, tmp_poly);
        for(i=1; i<l; i++)
            mult_mod_poly(polys_P[i], polys_P[i-1], tmp_poly);
    }
}


void free_data(){
    int i;
    for(i=0; i<POLY_DEG; i++)
        mpz_clear (gama_pow[i]);

    mpz_clear (modul_p);
}

