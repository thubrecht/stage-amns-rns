#include "../integers.h"

#include "structs_data.h"
#include "operations.h"
#include "functions.h"

#include <gmp.h>

#include "init.h"


int128_t polys_P[(NB_COEFF - 2)][NB_COEFF];

mpz_t modul_p;
mpz_t gama_pow[POLY_DEG];

void init_data(){
    int i;
    for(i=0; i<POLY_DEG; i++)
        mpz_init (gama_pow[i]);

    mpz_init (modul_p);


    mpz_set_str (modul_p, "5045007412108119992766317090765990042054377798089799072917179313841458553159336304392165596131934941378446443740611519627213357626813969449880758312252415830001735457742884017091800521824166574208465686665235054875723439685373", 10);

    mpz_set_str (gama_pow[0], "2265147477425827997657457146285425388859839587988986182165645540296431296943545504982939724207826111701781118487665527572104431807555793389171873148501561469474909946429141202375239852557914152017340478092215162946469514195661", 10);
    for(i=1; i<POLY_DEG; i++){
        mpz_mul (gama_pow[i], gama_pow[i-1], gama_pow[0]);
        mpz_mod (gama_pow[i], gama_pow[i], modul_p);
    }

    //~ IMPORTANT : initialisations above must be done before those below.
    compute_polys_P();
}


//~ computes representations of the polynomials P, used for conversion into the AMNS
void compute_polys_P(){
    int i, l;
    int128_t tmp_poly[NB_COEFF];

    //~ computation of a representation of 'phi*rho'
    from_mont_domain(tmp_poly, poly_P1);

    l = NB_COEFF - 2;
    if (l > 0){
        mult_mod_poly(polys_P[0], poly_P1, tmp_poly);
        for(i=1; i<l; i++)
            mult_mod_poly(polys_P[i], polys_P[i-1], tmp_poly);
    }
}


void free_data(){
    int i;
    for(i=0; i<POLY_DEG; i++)
        mpz_clear (gama_pow[i]);

    mpz_clear (modul_p);
}

