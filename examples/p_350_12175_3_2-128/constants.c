#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 14204386679090498285UL, .high = -5253564439740630L}, {.low = 9666001139432383611UL, .high = -2831169172074318L}, {.low = 2422113561397763190UL, .high = 1210610030347529L}};
int128_t poly_P1[NB_COEFF] = {{.low = 5831911440585039009UL, .high = -3389595832719116L}, {.low = 11095678818532948176UL, .high = -13911867483500L}, {.low = 1626038749335230100UL, .high = -633306970525224L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 6148013698313112783UL, .high = 1117940435363492L}, {.low = 13517830685375573500UL, .high = 2548024373586085L}, {.low = 6165605274919788139UL, .high = 3465163680474015L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 11828712263625129795UL, .high = 15242758967526386205UL}, {.low = 821783717897722770UL, .high = 2816480969554585325UL}, {.low = 18003937359943021977UL, .high = 4860628652296028007UL}};

