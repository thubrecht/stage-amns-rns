#include <gmp.h>

#include "../integers.h"

#include "operations.h"
#include "structs_data.h"

#ifndef USEFUL_FUNCTS
#define USEFUL_FUNCTS

void from_int_to_amns(int64_t* rop, mpz_t op);

void from_amns_to_int(mpz_t rop, int64_t* op);

int cmp_poly_evals(int64_t* pa, int64_t* pb);

void copy_poly(int64_t* rop, int64_t* op);

void add_lpoly(int128_t* rop, int128_t* pa, int128_t* pb);

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
__always_inline void scalar_mult_lpoly(int128_t* rop, int64_t* op, int64_t scalar) {
  for (int j = 0; j < NB_COEFF; j++) {
    rop[j] = mul_64(op[j], scalar);
  }
}

__always_inline void from_mont_domain(int64_t* rop, int64_t* op) {
  int128_t tmp[NB_COEFF];

  for (int i = 0; i < NB_COEFF; i++) {
    tmp[i] = grow_64(op[i]);
  }

  internal_reduction(rop, tmp);
}

void print_element(int64_t* poly);

#endif
