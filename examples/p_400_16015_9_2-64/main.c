#include <gmp.h>
#include <openssl/bn.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "operations.h"
#include "init.h"
#include "structs_data.h"
#include "functions.h"

#include "../integers.h"
#include "../timing.h"

#define BILLION 1000000000L

//~ Compilation command : make
//~ Execution command : ./main

//~ Important : polynomials representations form is P(X) = a0 + ... + an.X^n = (a0, ...,
// an).

int main(void) {
  mpz_t A, B, C, E, R;
  mpz_inits(A, B, C, E, R, NULL);

  BN_CTX* ctx = BN_CTX_new();
  BN_CTX_start(ctx);

  BIGNUM* opA = BN_CTX_get(ctx);
  BIGNUM* opB = BN_CTX_get(ctx);
  BIGNUM* opP = BN_CTX_get(ctx);
  BIGNUM* opAA = BN_CTX_get(ctx);
  BIGNUM* opBB = BN_CTX_get(ctx);
  BN_MONT_CTX* mont_ctx = BN_MONT_CTX_new();

  int64_t pa[NB_COEFF];
  int64_t pb[NB_COEFF];

  unsigned long seed = time(NULL);
  gmp_randstate_t r;
  gmp_randinit_default(r);
  gmp_randseed_ui(r, seed);

  init_data();

  mpz_urandomm(A, r, modul_p);
  mpz_urandomm(B, r, modul_p);
  mpz_set(E, A);

  BN_dec2bn(&opA, mpz_get_str(NULL, 10, A));
  BN_dec2bn(&opB, mpz_get_str(NULL, 10, B));
  BN_dec2bn(&opP, mpz_get_str(NULL, 10, modul_p));
  BN_copy(opAA, opA);
  BN_copy(opBB, opB);
  BN_MONT_CTX_set(mont_ctx, opP, ctx);

  uint64_t timer, mean_timer, time_start, time_end;
  uint64_t a_timer, mean_a_timer, a_time_start, a_time_end;
  uint64_t n_ins, mean_ins, n_ins_start, n_ins_end;

  /*----- Multiplication AMNS -----*/

  mean_timer = 0;
  mean_a_timer = 0;
  mean_ins = 0;

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
    mult_mod_poly(pa, pa, pb);
  }

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de données
    mpz_urandomm(A, r, modul_p);
    mpz_urandomm(B, r, modul_p);
    from_int_to_amns(pa, A);
    from_int_to_amns(pb, B);

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
        mult_mod_poly(pa, pa, pb);
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
        mult_mod_poly(pa, pa, pb);
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
        mult_mod_poly(pa, pa, pb);
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n", "AMNS",
         mean_timer / (N_SAMPLES * 100), mean_a_timer / (N_SAMPLES * 100),
         mean_ins / (N_SAMPLES * 100), (double)mean_ins / (double)mean_a_timer);

  /*----- Multiplication GMP -----*/

  mean_timer = 0;
  mean_a_timer = 0;
  mean_ins = 0;

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
    mpz_mul(A, A, B);
    mpz_mod(A, A, modul_p);
  }

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de données
    mpz_urandomm(A, r, modul_p);
    mpz_urandomm(B, r, modul_p);

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
        mpz_mul(A, A, B);
        mpz_mod(A, A, modul_p);
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
        mpz_mul(A, A, B);
        mpz_mod(A, A, modul_p);
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
        mpz_mul(A, A, B);
        mpz_mod(A, A, modul_p);
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n", "GMP",
         mean_timer / (N_SAMPLES * 100), mean_a_timer / (N_SAMPLES * 100),
         mean_ins / (N_SAMPLES * 100), (double)mean_ins / (double)mean_a_timer);

  /*----- Multiplication OpenSSL Montgommery -----*/

  mean_timer = 0;
  mean_a_timer = 0;
  mean_ins = 0;

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
    BN_mod_mul_montgomery(opA, opA, opB, mont_ctx, ctx);
  }

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de données
    mpz_urandomm(A, r, modul_p);
    mpz_urandomm(B, r, modul_p);
    BN_dec2bn(&opA, mpz_get_str(NULL, 10, A));
    BN_dec2bn(&opB, mpz_get_str(NULL, 10, B));
    BN_to_montgomery(opA, opA, mont_ctx, ctx);
    BN_to_montgomery(opB, opB, mont_ctx, ctx);

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul_montgomery(opA, opA, opB, mont_ctx, ctx);
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul_montgomery(opA, opA, opB, mont_ctx, ctx);
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul_montgomery(opA, opA, opB, mont_ctx, ctx);
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n",
         "OpenSSL Montgommery", mean_timer / (N_SAMPLES * 100),
         mean_a_timer / (N_SAMPLES * 100), mean_ins / (N_SAMPLES * 100),
         (double)mean_ins / (double)mean_a_timer);

  /*----- Multiplication OpenSSL -----*/

  mean_timer = 0;
  mean_a_timer = 0;
  mean_ins = 0;

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
    BN_mod_mul(opAA, opAA, opBB, opP, ctx);
  }

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de données
    mpz_urandomm(A, r, modul_p);
    mpz_urandomm(B, r, modul_p);
    BN_dec2bn(&opAA, mpz_get_str(NULL, 10, A));
    BN_dec2bn(&opBB, mpz_get_str(NULL, 10, B));

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul(opAA, opAA, opBB, opP, ctx);
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul(opAA, opAA, opBB, opP, ctx);
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
        BN_mod_mul(opAA, opAA, opBB, opP, ctx);
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n", "OpenSSL",
         mean_timer / (N_SAMPLES * 100), mean_a_timer / (N_SAMPLES * 100),
         mean_ins / (N_SAMPLES * 100), (double)mean_ins / (double)mean_a_timer);

  printf("\n");
  gmp_printf("A       : %Zd\n", A);
  gmp_printf("B       : %Zd\n\n", B);

  // GMP
  mpz_mul(R, A, B);
  mpz_mod(R, R, modul_p);

  gmp_printf("r_gmp   : %Zd\n", R);

  // AMNS
  from_int_to_amns(pa, R);
  /* from_int_to_amns(pb, B); */
  /* mult_mod_poly(pa, pa, pb); */
  from_amns_to_int(R, pa);

  gmp_printf("r_amns  : %Zd\n", R);

  // OpenSSL
  BN_dec2bn(&opAA, mpz_get_str(NULL, 10, A));
  BN_dec2bn(&opBB, mpz_get_str(NULL, 10, B));
  BN_mod_mul(opAA, opAA, opBB, opP, ctx);

  gmp_printf("r_ssld  : %s\n", BN_bn2dec(opAA));

  // OpenSSL Montgommery
  BN_dec2bn(&opA, mpz_get_str(NULL, 10, A));
  BN_dec2bn(&opB, mpz_get_str(NULL, 10, B));
  BN_to_montgomery(opA, opA, mont_ctx, ctx);
  BN_to_montgomery(opB, opB, mont_ctx, ctx);
  BN_mod_mul_montgomery(opA, opA, opB, mont_ctx, ctx);

  BN_from_montgomery(opA, opA, mont_ctx, ctx);

  gmp_printf("r_sslm  : %s\n", BN_bn2dec(opA));

  // Vidage
  mpz_clears(A, B, C, E, R, NULL);
  gmp_randclear(r);

  BN_MONT_CTX_free(mont_ctx);
  BN_CTX_end(ctx);
  BN_CTX_free(ctx);

  free_data();
  return 0;
}