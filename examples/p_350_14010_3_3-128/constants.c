#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 15668594879876467810UL, .high = -7599600721557525L}, {.low = 214431431763333843UL, .high = -3128431511819486L}, {.low = 4923521454690522318UL, .high = -1101628328937366L}};
int128_t poly_P1[NB_COEFF] = {{.low = 16972516410162073115UL, .high = -6910155391078837L}, {.low = 12868050691727750790UL, .high = -7457798960228482L}, {.low = 6916754972282287657UL, .high = -747197682119845L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 66254881908902501UL, .high = 755625571797122L}, {.low = 15790627842179289904UL, .high = 563396879329440L}, {.low = 7259192093267736508UL, .high = 2867980460448758L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 12566689955978459181UL, .high = 3233131469363538019UL}, {.low = 14985572199014383264UL, .high = 2276365559849442933UL}, {.low = 8079075916229619940UL, .high = 12069429524724603961UL}};

