#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 9850777350189523899UL, .high = 50452639878614542L}, {.low = 2582896029978189067UL, .high = 72207693741794310L}, {.low = 2966956721720846615UL, .high = 8888832317307105L}};
int128_t poly_P1[NB_COEFF] = {{.low = 1751382233731523906UL, .high = 65500743341542837L}, {.low = 5110346726048304523UL, .high = 16193601141530333L}, {.low = 7650913556815183286UL, .high = 25060960313777570L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 11197655691094766299UL, .high = 2449985546799481L}, {.low = 11798047850635809154UL, .high = 2397661658737703L}, {.low = 735422010097046620UL, .high = 43292588609888789L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 12593045330859619587UL, .high = 5733678334625881869UL}, {.low = 10436015358892630962UL, .high = 360257435173231263UL}, {.low = 14818433112599330792UL, .high = 4023255161607027984UL}};

