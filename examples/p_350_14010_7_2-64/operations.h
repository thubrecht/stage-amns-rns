#include "../integers.h"

#include "structs_data.h"

#ifndef POLY_MULT_ADD
#define POLY_MULT_ADD


void sub_poly(int64_t *rop, int64_t *pa, int64_t *pb);
void add_poly(int64_t *rop, int64_t *pa, int64_t *pb);
void neg_poly(int64_t *rop, int64_t *op);
void scalar_mult_poly(int64_t *rop, int64_t *op, int64_t scalar);
void double_poly_coeffs(int64_t *rop, int64_t *op);
void lshift_poly_coeffs(int64_t *rop, int64_t *op, int nb_pos);

void mult_mod_poly(int64_t *rop, int64_t *pa, int64_t *pb);

void square_mod_poly(int64_t *rop, int64_t *pa);

__always_inline void internal_reduction(int64_t *rop, int128_t *op) {
    uint64_t tmp_q[NB_COEFF];
    int128_t tmp_zero[NB_COEFF];

    //~ computation of: op*neg_inv_ri_rep_coeff mod((X^n - c), mont_phi)
    uint64_t q_coeff = 4562063637600877480UL;
    tmp_q[0] = addu_64(scalu_64(op[0].low, int_red_coeff_inv[0]), shift_by_1u_64(addu_64(addu_64(addu_64(addu_64(addu_64(scalu_64(op[1].low, int_red_coeff_inv[6]), scalu_64(op[2].low, int_red_coeff_inv[5])), scalu_64(op[3].low, int_red_coeff_inv[4])), scalu_64(op[4].low, int_red_coeff_inv[3])), scalu_64(op[5].low, int_red_coeff_inv[2])), scalu_64(op[6].low, int_red_coeff_inv[1]))));
    tmp_q[1] = addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[1]), scalu_64(op[1].low, int_red_coeff_inv[0])), shift_by_1u_64(addu_64(addu_64(addu_64(addu_64(scalu_64(op[2].low, int_red_coeff_inv[6]), scalu_64(op[3].low, int_red_coeff_inv[5])), scalu_64(op[4].low, int_red_coeff_inv[4])), scalu_64(op[5].low, int_red_coeff_inv[3])), scalu_64(op[6].low, int_red_coeff_inv[2]))));
    tmp_q[2] = addu_64(addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[2]), scalu_64(op[1].low, int_red_coeff_inv[1])), scalu_64(op[2].low, int_red_coeff_inv[0])), shift_by_1u_64(addu_64(addu_64(addu_64(scalu_64(op[3].low, int_red_coeff_inv[6]), scalu_64(op[4].low, int_red_coeff_inv[5])), scalu_64(op[5].low, int_red_coeff_inv[4])), scalu_64(op[6].low, int_red_coeff_inv[3]))));
    tmp_q[3] = addu_64(addu_64(addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[3]), scalu_64(op[1].low, int_red_coeff_inv[2])), scalu_64(op[2].low, int_red_coeff_inv[1])), scalu_64(op[3].low, int_red_coeff_inv[0])), shift_by_1u_64(addu_64(addu_64(scalu_64(op[4].low, int_red_coeff_inv[6]), scalu_64(op[5].low, int_red_coeff_inv[5])), scalu_64(op[6].low, int_red_coeff_inv[4]))));
    tmp_q[4] = addu_64(addu_64(addu_64(addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[4]), scalu_64(op[1].low, int_red_coeff_inv[3])), scalu_64(op[2].low, int_red_coeff_inv[2])), scalu_64(op[3].low, int_red_coeff_inv[1])), scalu_64(op[4].low, int_red_coeff_inv[0])), shift_by_1u_64(addu_64(scalu_64(op[5].low, int_red_coeff_inv[6]), scalu_64(op[6].low, int_red_coeff_inv[5]))));
    tmp_q[5] = addu_64(addu_64(addu_64(addu_64(addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[5]), scalu_64(op[1].low, int_red_coeff_inv[4])), scalu_64(op[2].low, int_red_coeff_inv[3])), scalu_64(op[3].low, int_red_coeff_inv[2])), scalu_64(op[4].low, int_red_coeff_inv[1])), scalu_64(op[5].low, int_red_coeff_inv[0])), scalu_64(op[6].low, q_coeff));
    tmp_q[6] = addu_64(addu_64(addu_64(addu_64(addu_64(addu_64(scalu_64(op[0].low, int_red_coeff_inv[6]), scalu_64(op[1].low, int_red_coeff_inv[5])), scalu_64(op[2].low, int_red_coeff_inv[4])), scalu_64(op[3].low, int_red_coeff_inv[3])), scalu_64(op[4].low, int_red_coeff_inv[2])), scalu_64(op[5].low, int_red_coeff_inv[1])), scalu_64(op[6].low, int_red_coeff_inv[0]));

    //~ computation of: tmp_q*red_int_coeff mod(X^n - c)
    int64_t z_coeff = 1062898980641628UL;

    tmp_zero[0] = add_128(neg_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[0])), shift_by_1_128(add_128(add_128(add_128(add_128(add_128(mul_64((int64_t)tmp_q[1], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[2], int_red_coeff_0[5])), neg_128(mul_64((int64_t)tmp_q[3], int_red_coeff_0[4]))), neg_128(mul_64((int64_t)tmp_q[4], int_red_coeff_0[3]))), mul_64((int64_t)tmp_q[5], int_red_coeff_0[2])), neg_128(mul_64((int64_t)tmp_q[6], int_red_coeff_0[1])))));
    tmp_zero[1] = add_128(add_128(neg_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[1])), neg_128(mul_64((int64_t)tmp_q[1], int_red_coeff_0[0]))), shift_by_1_128(add_128(add_128(add_128(add_128(mul_64((int64_t)tmp_q[2], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[3], int_red_coeff_0[5])), neg_128(mul_64((int64_t)tmp_q[4], int_red_coeff_0[4]))), neg_128(mul_64((int64_t)tmp_q[5], int_red_coeff_0[3]))), mul_64((int64_t)tmp_q[6], int_red_coeff_0[2]))));
    tmp_zero[2] = add_128(add_128(add_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[2]), neg_128(mul_64((int64_t)tmp_q[1], int_red_coeff_0[1]))), neg_128(mul_64((int64_t)tmp_q[2], int_red_coeff_0[0]))), shift_by_1_128(add_128(add_128(add_128(mul_64((int64_t)tmp_q[3], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[4], int_red_coeff_0[5])), neg_128(mul_64((int64_t)tmp_q[5], int_red_coeff_0[4]))), neg_128(mul_64((int64_t)tmp_q[6], int_red_coeff_0[3])))));
    tmp_zero[3] = add_128(add_128(add_128(add_128(neg_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[3])), mul_64((int64_t)tmp_q[1], int_red_coeff_0[2])), neg_128(mul_64((int64_t)tmp_q[2], int_red_coeff_0[1]))), neg_128(mul_64((int64_t)tmp_q[3], int_red_coeff_0[0]))), shift_by_1_128(add_128(add_128(mul_64((int64_t)tmp_q[4], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[5], int_red_coeff_0[5])), neg_128(mul_64((int64_t)tmp_q[6], int_red_coeff_0[4])))));
    tmp_zero[4] = add_128(add_128(add_128(add_128(add_128(neg_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[4])), neg_128(mul_64((int64_t)tmp_q[1], int_red_coeff_0[3]))), mul_64((int64_t)tmp_q[2], int_red_coeff_0[2])), neg_128(mul_64((int64_t)tmp_q[3], int_red_coeff_0[1]))), neg_128(mul_64((int64_t)tmp_q[4], int_red_coeff_0[0]))), shift_by_1_128(add_128(mul_64((int64_t)tmp_q[5], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[6], int_red_coeff_0[5]))));
    tmp_zero[5] = add_128(add_128(add_128(add_128(add_128(add_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[5]), neg_128(mul_64((int64_t)tmp_q[1], int_red_coeff_0[4]))), neg_128(mul_64((int64_t)tmp_q[2], int_red_coeff_0[3]))), mul_64((int64_t)tmp_q[3], int_red_coeff_0[2])), neg_128(mul_64((int64_t)tmp_q[4], int_red_coeff_0[1]))), neg_128(mul_64((int64_t)tmp_q[5], int_red_coeff_0[0]))), mul_64((int64_t)tmp_q[6], z_coeff));
    tmp_zero[6] = add_128(add_128(add_128(add_128(add_128(add_128(mul_64((int64_t)tmp_q[0], int_red_coeff_0[6]), mul_64((int64_t)tmp_q[1], int_red_coeff_0[5])), neg_128(mul_64((int64_t)tmp_q[2], int_red_coeff_0[4]))), neg_128(mul_64((int64_t)tmp_q[3], int_red_coeff_0[3]))), mul_64((int64_t)tmp_q[4], int_red_coeff_0[2])), neg_128(mul_64((int64_t)tmp_q[5], int_red_coeff_0[1]))), neg_128(mul_64((int64_t)tmp_q[6], int_red_coeff_0[0])));

    //~ computation of: (op + tmp_zero)/mont_phi
    rop[0] = add_128(op[0], tmp_zero[0]).high;
    rop[1] = add_128(op[1], tmp_zero[1]).high;
    rop[2] = add_128(op[2], tmp_zero[2]).high;
    rop[3] = add_128(op[3], tmp_zero[3]).high;
    rop[4] = add_128(op[4], tmp_zero[4]).high;
    rop[5] = add_128(op[5], tmp_zero[5]).high;
    rop[6] = add_128(op[6], tmp_zero[6]).high;
}

void exact_coeffs_reduction(int64_t *rop, int64_t *op);

#endif

