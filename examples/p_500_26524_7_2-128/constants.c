#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 8596658362999374013UL, .high = -73L}, {.low = 5282245255947045024UL, .high = -70L}, {.low = 15114154898686317252UL, .high = 16L}, {.low = 2108577659595949052UL, .high = -123L}, {.low = 5398062912161715247UL, .high = -82L}, {.low = 7391293811331542326UL, .high = 24L}, {.low = 4600689156239820470UL, .high = -89L}};
int128_t poly_P1[NB_COEFF] = {{.low = 12633611210746715917UL, .high = 22L}, {.low = 5153066515465160769UL, .high = -137L}, {.low = 8450599679279124914UL, .high = 58L}, {.low = 7763635902075753758UL, .high = -127L}, {.low = 18167160622821741125UL, .high = -40L}, {.low = 5448984744293241405UL, .high = -75L}, {.low = 15531159973636472890UL, .high = -89L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 15861535298887551105UL, .high = 28L}, {.low = 14589558634328809702UL, .high = 4L}, {.low = 5099786535090937284UL, .high = 98L}, {.low = 12648316681198276750UL, .high = 93L}, {.low = 14637454785642562785UL, .high = 14L}, {.low = 7169704096065344080UL, .high = 19L}, {.low = 14024602457729306930UL, .high = 45L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 14493831420988836431UL, .high = 11219650961227969562UL}, {.low = 13144141940747554920UL, .high = 10176902365998782714UL}, {.low = 6194859703540090432UL, .high = 17272359105421250255UL}, {.low = 12905435267709605950UL, .high = 2344754310199798288UL}, {.low = 1082381990952360283UL, .high = 17402326689415350378UL}, {.low = 13866520204332168922UL, .high = 2894742219286412760UL}, {.low = 18107473154465506898UL, .high = 14368475099063514130UL}};

