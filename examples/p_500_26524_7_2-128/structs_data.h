#include "../integers.h"

#include <gmp.h>

#ifndef STRUCTS_DATA
#define STRUCTS_DATA


//~ IMPORTANT : We take 'phi = 1 << WORD_SIZE'
#define WORD_SIZE 128
#define POLY_DEG 6
#define NB_COEFF 7
#define NB_ADD_MAX 0

#define RHO_LOG2 76  // rho = 1 << RHO_LOG2.

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
extern int128_t poly_P0[NB_COEFF];
extern int128_t poly_P1[NB_COEFF];

//~ representations of polynomials Pi, for i=2,...,n-1
extern int128_t polys_P[(NB_COEFF - 2)][NB_COEFF];

//~ representation of the coefficient used in the internal reduction
extern int128_t int_red_coeff_0[NB_COEFF];

extern uint128_t int_red_coeff_inv[NB_COEFF];

extern mpz_t modul_p;
extern mpz_t gama_pow[POLY_DEG];

#endif
