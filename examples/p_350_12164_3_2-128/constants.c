#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 13382531796252331522UL, .high = -851938913511445L}, {.low = 9026040241623627697UL, .high = 1966433863164322L}, {.low = 14121454101353533997UL, .high = -2448839847428549L}};
int128_t poly_P1[NB_COEFF] = {{.low = 11293804496110223904UL, .high = -8246254762263684L}, {.low = 11593921533006870870UL, .high = -604885528711455L}, {.low = 12827517505077784121UL, .high = 348151331338648L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 13870231867478029617UL, .high = 4119367839450514L}, {.low = 11179380325817835758UL, .high = 4268817137750561L}, {.low = 5768444763168491845UL, .high = 988056374981475L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 1877413543585134547UL, .high = 8901550151975088786UL}, {.low = 12760255925318068544UL, .high = 17506332817164975051UL}, {.low = 17886581319139850895UL, .high = 6012091415996490087UL}};

