#include <gmp.h>

#include "../integers.h"

#include "operations.h"
#include "structs_data.h"

#ifndef USEFUL_FUNCTS
#define USEFUL_FUNCTS

void from_int_to_amns(int128_t* rop, mpz_t op);

void from_amns_to_int(mpz_t rop, int128_t* op);

int cmp_poly_evals(int128_t* pa, int128_t* pb);

void copy_poly(int128_t* rop, int128_t* op);

void add_lpoly(int256_t* rop, int256_t* pa, int256_t* pb);

//~ assumes 'scalar' and/or coeffs of 'op' small enough to avoid an overflow.
__always_inline void scalar_mult_lpoly(int256_t* rop, int128_t* op, int128_t scalar) {
  for (int j = 0; j < NB_COEFF; j++) {
    rop[j] = mul_128(op[j], scalar);
  }
}

__always_inline void from_mont_domain(int128_t* rop, int128_t* op) {
  int256_t tmp[NB_COEFF];

  for (int i = 0; i < NB_COEFF; i++) {
    tmp[i] = grow_128(op[i]);
  }

  internal_reduction(rop, tmp);
}

void print_element(int128_t* poly);

#endif
