#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 11356940537204238661UL, .high = 42399868415L}, {.low = 10574010718485519549UL, .high = 32547324858L}, {.low = 12084212946364051749UL, .high = 31086931387L}};
int128_t poly_P1[NB_COEFF] = {{.low = 3465674412118289888UL, .high = 36949120764L}, {.low = 9451505925309247908UL, .high = 25935485652L}, {.low = 10706288901128156884UL, .high = 27211470595L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 5381910201647303037UL, .high = 21047863450L}, {.low = 1335876817969541941UL, .high = 36727164337L}, {.low = 371427588542977397UL, .high = 23834673378L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 681740443759547183UL, .high = 3870504278274801800UL}, {.low = 6851567284762814155UL, .high = 4956888915708324162UL}, {.low = 13524817146575606314UL, .high = 10810666651950997967UL}};

