#include "../integers.h"

#include "structs_data.h"

//~ representations of the polynomials P0 and P1, used for conversion into the AMNS
int128_t poly_P0[NB_COEFF] = {{.low = 5416906860986385227UL, .high = 66905L}, {.low = 1317809750069514615UL, .high = 86098L}, {.low = 11727834843545533824UL, .high = 65979L}, {.low = 11228419104663555501UL, .high = 71984L}, {.low = 1677377750358813891UL, .high = 56193L}};
int128_t poly_P1[NB_COEFF] = {{.low = 3225317624199748556UL, .high = 87978L}, {.low = 13395496336295234613UL, .high = 32943L}, {.low = 13346616767484157568UL, .high = 58770L}, {.low = 930245670097768589UL, .high = 48812L}, {.low = 10727247588210550135UL, .high = 19715L}};

//~ representation of the coefficient used in the internal reduction
int128_t int_red_coeff_0[NB_COEFF] = {{.low = 5027242486973553285UL, .high = 45898L}, {.low = 8793504827248296702UL, .high = 636L}, {.low = 9696779821061447322UL, .high = 7784L}, {.low = 15657241005118274797UL, .high = 28154L}, {.low = 4627174070577676535UL, .high = 4180L}};

uint128_t int_red_coeff_inv[NB_COEFF] = {{.low = 6668411813914236799UL, .high = 1596367483383773860UL}, {.low = 16086472449795577104UL, .high = 11021443482151097565UL}, {.low = 7180403309044331306UL, .high = 4173962867222843496UL}, {.low = 16302558365099006815UL, .high = 7596608332606512100UL}, {.low = 1375891477059116501UL, .high = 2682806992176831819UL}};

