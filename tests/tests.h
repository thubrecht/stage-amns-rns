#ifndef _TESTS_H
#define _TESTS_H

#include <stdint.h>
#include <stdio.h>

#define N_RANDOM 5000

inline static void bin(uint64_t n) {
  uint64_t i;
  for (i = (uint64_t)1 << 63; i > 0; i = i / 2)
    (n & i) ? printf("1") : printf("0");
}

inline static unsigned long get_seed() {
  unsigned long seed[1];

  FILE* src = fopen("/dev/random", "r");
  size_t read = fread(seed, sizeof(long), 1, src);
  fclose(src);

  return seed[0];
}

void test_64();
void test_128();
void test_256();
void test_512();
void test_192();
void test_394();

int main(void);

#endif
