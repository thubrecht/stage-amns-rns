#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>

#include "../convert.h"
#include "../integers.h"
#include "tests.h"

void test_64(void) {
  printf("Testing 64-bit operations\t");
  // On tire N_RANDOM entiers de 64 bits, et on vérifie que les opérations
  // donnent le bon résultat

  gmp_randstate_t r_state;
  mpz_t A, B, C, D, _R;

  mpz_inits(A, B, C, D, _R, NULL);

  int64_t a, b, c;
  int128_t d;

  // Initialisation
  gmp_randinit_default(r_state);
  gmp_randseed_ui(r_state, get_seed());

  // Tirages et tests
  for (int i = 0; i < N_RANDOM; i++) {
    mpz_urandomb(A, r_state, 63);
    mpz_urandomb(B, r_state, 63);

    a = mpz_to_64(A);
    b = mpz_to_64(B);

    // Addition
    c = add_64(a, b);
    mpz_add(C, A, B);
    if (cmp_64(c, mpz_to_64(C))) {
      gmp_printf("\nA : GMP %Zd; Custom %lld\n", A, a);
      gmp_printf("Erreur addition : GMP %Zd; Custom %lld\n", C, c);
      exit(1);
    }

    // Soustraction
    c = sub_64(a, b);
    mpz_sub(C, A, B);
    if (cmp_64(c, mpz_to_64(C))) {
      gmp_printf("\nA : GMP %Zd; Custom %lld\n", A, a);
      gmp_printf("Erreur soustraction : GMP %Zd; Custom %lld\n", C, c);
      exit(1);
    }

    // Multiplication
    d = neg_128(mul_64(neg_64(a), b));
    mpz_mul(D, A, B);
    if (cmp_128(d, mpz_to_128(D))) {
      gmp_printf("\nA : GMP %Zd; Custom %lld\n", A, a);
      mpz_from_128(_R, d);
      gmp_printf("Erreur multiplication : GMP %Zd; Custom %Zd\n", D, _R);
      bin(d.high);
      printf(" ");
      bin(d.low);
      printf("\n");
      bin(mpz_to_128(D).high);
      printf(" ");
      bin(mpz_to_128(D).low);
      printf("\n");

      exit(1);
    }

    // Shifts
    /* c = shift_by_1_64(a); */

    if (!(i % (N_RANDOM / 40))) {
      printf(".");
    }
  }

  // Vidage de la mémoire
  mpz_clears(A, B, C, D, NULL);
  gmp_randclear(r_state);

  printf("[ok]\n");
}

void test_128(void) {
  printf("Testing 128-bit operations\t");
  // On tire N_RANDOM entiers de 128 bits, et on vérifie que les opérations
  // donnent le bon résultat

  gmp_randstate_t r_state;
  mpz_t A, B, C, D, _R;

  mpz_inits(A, B, C, D, _R, NULL);

  int128_t a, b, c;
  int256_t d;

  // Initialisation
  gmp_randinit_default(r_state);
  gmp_randseed_ui(r_state, get_seed());

  // Tirages et tests
  for (int i = 0; i < N_RANDOM; i++) {
    mpz_urandomb(A, r_state, 127);
    mpz_urandomb(B, r_state, 127);

    a = mpz_to_128(A);
    b = mpz_to_128(B);

    // Négation
    c = neg_128(neg_128(a));
    if (cmp_128(c, a)) {
      mpz_from_128(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_128(_R, c);
      gmp_printf("Erreur négation : GMP %Zd; Custom %Zd\n", A, _R);
      exit(1);
    }

    // Addition
    c = add_128(a, b);
    mpz_add(C, A, B);
    if (cmp_128(c, mpz_to_128(C))) {
      gmp_printf("\nA : GMP %Zd; Custom %lld\n", A, a);
      gmp_printf("Erreur addition : GMP %Zd; Custom %lld\n", C, c);
      exit(1);
    }

    // Soustraction

    c = sub_128(a, b);
    mpz_sub(C, A, B);
    if (cmp_128(c, mpz_to_128(C))) {
      mpz_from_128(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_128(_R, b);
      gmp_printf("B : GMP %Zd; Custom %Zd\n", B, _R);
      mpz_from_128(_R, c);
      gmp_printf("Erreur soustraction : GMP %Zd; Custom %Zd\n", C, _R);
      exit(1);
    }

    // Multiplication
    d = mul_128(a, b);
    mpz_mul(D, A, B);
    if (cmp_256(d, mpz_to_256(D))) {
      mpz_from_128(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_256(_R, d);
      gmp_printf("Erreur multiplication : GMP %Zd; Custom %Zd\n", D, _R);
      exit(1);
    }

    // Shifts

    if (!(i % (N_RANDOM / 40))) {
      printf(".");
    }
  }

  // Vidage de la mémoire
  mpz_clears(A, B, C, D, NULL);
  gmp_randclear(r_state);

  printf("[ok]\n");
}

void test_256(void) {
  printf("Testing 256-bit operations\t");
  // On tire N_RANDOM entiers de 256 bits, et on vérifie que les opérations
  // donnent le bon résultat

  gmp_randstate_t r_state;
  mpz_t A, B, C, D, _R;

  mpz_inits(A, B, C, D, _R, NULL);

  int256_t a, b, c;
  int512_t d;

  // Initialisation
  gmp_randinit_default(r_state);
  gmp_randseed_ui(r_state, get_seed());

  // Tirages et tests
  for (int i = 0; i < N_RANDOM; i++) {
    mpz_urandomb(A, r_state, 255);
    mpz_urandomb(B, r_state, 255);

    a = mpz_to_256(A);
    b = mpz_to_256(B);

    // Négation
    c = neg_256(neg_256(a));
    if (cmp_256(c, a)) {
      mpz_from_256(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_256(_R, c);
      gmp_printf("Erreur négation : GMP %Zd; Custom %Zd\n", A, _R);
      exit(1);
    }

    // Addition
    c = add_256(a, b);
    mpz_add(C, A, B);
    if (cmp_256(c, mpz_to_256(C))) {
      mpz_from_256(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_256(_R, b);
      gmp_printf("B : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_256(_R, c);
      gmp_printf("Erreur addition : GMP %Zd; Custom %Zd\n", C, _R);
      exit(1);
    }

    // Soustraction

    c = sub_256(a, b);
    mpz_sub(C, A, B);
    if (cmp_256(c, mpz_to_256(C))) {
      mpz_from_256(_R, a);
      gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R);
      mpz_from_256(_R, b);
      gmp_printf("B : GMP %Zd; Custom %Zd\n", B, _R);
      mpz_from_256(_R, c);
      gmp_printf("Erreur soustraction : GMP %Zd; Custom %Zd\n", C, _R);
      exit(1);
    }

    // Multiplication
    /* d = mul_256(a, b); */
    /* mpz_mul(D, A, B); */
    /* if (cmp_512(d, mpz_to_512(D))) { */
    /*   mpz_from_256(_R, a); */
    /*   gmp_printf("\nA : GMP %Zd; Custom %Zd\n", A, _R); */
    /*   mpz_from_256(_R, b); */
    /*   gmp_printf("B : GMP %Zd; Custom %Zd\n", A, _R); */
    /*   mpz_from_512(_R, d); */
    /*   gmp_printf("Erreur multiplication : GMP %Zd; Custom %Zd\n", D, _R); */
    /*   exit(1); */
    /* } */

    // Shifts

    if (!(i % (N_RANDOM / 40))) {
      printf(".");
    }
  }

  // Vidage de la mémoire
  mpz_clears(A, B, C, D, NULL);
  gmp_randclear(r_state);

  printf("[ok]\n");
}

int main(void) {
  setbuf(stdout, NULL);
  test_64();
  test_128();
  test_256();

  return 0;
}
