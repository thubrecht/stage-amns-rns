\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[a4paper]{geometry}
\usepackage{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{txfonts}

\usepackage{listings}

\usepackage{float}

\usepackage{multicol}

\usepackage[
backend=biber,
style=numeric,
sorting=ynt
]{biblatex}
\addbibresource{bibliographie.bib}

\lstset{
    language=C,
    basicstyle=\ttfamily\small,
    numberstyle=\footnotesize,
    numbers=left,
    tabsize=2,
}

\makeatletter
\newcommand*{\centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother


\title{Arithmétique multiprécision dans le cadre de l'AMNS}
\author{Tom Hubrecht}
\date{}

\begin{document}

\maketitle

\tableofcontents

\newpage

\renewcommand{\arraystretch}{1.3}

\section{Introduction}
\subsection{Contexte de travail}
Mon stage s'est déroulé en présentiel dans le laboratoire IMATH de l'Université de Toulon entre le 31 mai et le 30 juillet. J'étais encadré par Laurent-Stéphane Didier, mais j'ai également beaucoup échangé avec Jean-Marc Robert et Yssouf Dosso. La librairie créée lors de ce stage est disponible à \url{https://git.eleves.ens.fr/thubrecht/stage-amns-rns}, et le code pour la réduction de réseau en norme infinie à \url{https://github.com/Tom-Hubrecht/reduction}.

\subsection{AMNS}
L'Adapted Modular Number System \cite{Didier2020} est un système de représentation qui vise à accélérer des calculs entiers modulo un nombre premier $p$. Un AMNS est défini par un tuple $(p, n, \gamma, \rho, E)$. Dans ce système, pour tout entier $x < p$, il existe un vecteur $V_x = (v_0,\ldots,v_{n-1})$ avec $|v_i| < \rho$ tel que $x = \sum_{i = 0}^{n - 1}v_i\gamma^i \mod p$, on peut donc assimiler $x$ à $V_x$ et $V_x$ au polynôme $\sum_{i = 0}^{n - 1}v_i X^i$. Ainsi, dans ce système de représentation, les opérations d'addition, soustraction, multiplication s'effectuent sur des polynômes. Cependant, on souhaite également que les conditions deg$(V_x) < n$ et $v_i < \rho$ soient toujours vérifiées lorsque l'on effectue des calculs. Pour cela, on a besoin de deux opérations de réduction. La réduction externe permettant de diminuer le degré du polynôme est une division euclidienne de $V_x$ par $E = X^n - \lambda$ où $\lambda = \gamma^n$, ainsi on garde $V_x(\gamma) = x$. La réduction interne sert à trouver un polynôme $V'_x$ dont les coefficients sont inférieurs à $\rho$ et tel que $V'_x(\gamma) = V_x(\gamma) \mod p$.

L'addition de deux nombres $a, b$ est alors l'addition des deux polynômes les représentant $A, B$ coefficient par coefficient. La multiplication est une multiplication des deux polynômes suivie d'une réduction externe.

Dans la pratique, ce systèmes est intéressant car il permet de ne pas manipuler les représentations binaires de grands nombres directement. En effet, les ordinateurs classiques utilisent des registres de 64 bits, ce qui oblige à fractionner les grands entiers (d'une taille de plusieurs centaines de bits) pour les manipuler. En choisissant un AMNS avec $\rho < 2^{64}$, cela permet d'utiliser des coefficients qui sont manipulables directement. Cependant, pour un entier d'une taille donnée, il y a un compromis entre diminuer la taille des coefficients et diminuer le degré des polynomes manipulés, puisque les opérations ne se font pas en temps constant, mais que l'on n'est pas encore dans le domaine de la complexité asymptotique. Une approche serait donc d'utiliser des coefficients prenant plusieurs mots machines, et ainsi diminuer le degré maximal des polynômes manipulés. Mon travail lors de ce stage était donc de chercher si l'on pouvait trouver un compromis à ce niveau là.

\section{Arithmétique multiprécision}

\subsection{Introduction}
L'arithmétique multiprécision permet de manipuler des entiers de taille supérieure à ce qui est disponible au niveau matériel (e.g. des entiers de 64 bits sur une architecture ARM 32 bit, ou des entiers de taille 128 bits), une librairie très utilisée dans ce domaine est GMP, qui permet d'utiliser des entiers de taille arbitraire.

Le travail fut de créer une librairie en C pour implanter une arithmétique multiprécision de taille fixe, en introduisant des types d'entiers avec des tailles de 128, 256 ou 512 bits ainsi que les opérations élémentaires dessus (addition, soustraction et multiplication). Cela permet d'abstraire les capacités réelles du processeur utilisé et donc d'avoir des multiplications totales sur 64 bits sur un processeur ARM par exemple.

\subsection{Implantation d'une librairie multiprécision}
J'ai construit une librairie en C pour manipuler des entiers de taille variant entre 64 et 512 bits, pour cela, j'ai défini les types correspondants, en gardant la même convention que celle utilisée dans \texttt{<stdint.h>}, c'est à dire les noms sont « int128\_t », « uint128\_t », « int256\_t », etc. J'ai également implanté les opérations de base nécessaires, l'addition, la soustraction, la multiplication, les décalages à gauche et à droite, ainsi que certaines opérations bit à bit.

La structure des grands entiers est simplement une juxtaposition d'entiers 64 bits :

\begin{figure}[H]
\begin{multicols}{2}
 \begin{verbatim}
typedef union {
  struct {
    uint64_t low;
    int64_t high;
  };
#ifdef __SIZEOF_INT128__
  __int128 full;
#endif
} int128_t;
\end{verbatim}
\newpage

\begin{verbatim}
typedef union {
  struct {
    uint64_t l_low;
    uint64_t l_high;
    uint64_t h_low;
    int64_t h_high;
  };
#ifdef __SIZEOF_INT128__
  struct {
    unsigned __int128 f_low;
    __int128 f_high;
  };
#endif
  struct {
    uint128_t low;
    int128_t high;
  };
} int256_t;
\end{verbatim}
\end{multicols}
\caption{Structures d'entiers 128 et 256 bits}
\end{figure}

La multiplication est faite selon l'algorithme classique de complexité quadratique, car l'algorithme de Karatsuba rajoute des additions et des tests, ce qui ralentit fortement les calculs au vu de la taille des données manipulées.

Cette librairie permet également une abstraction de l'architecture réellement utilisée, puisque sur une architecture ARM, la multiplication matérielle ne permet que d'obtenir la partie basse du résultat, mais l'implantation de \texttt{mul\_64} renvoie un résultat de type \texttt{int128\_t} sur ARM et sur x86, de cette manière, on peut utiliser des coefficients de 64 bits sur une architecture ARM, alors que précédemment, il était nécessaire de se limiter à des coefficients de 32 bits.

\subsection{Résultats}
Pour évaluer l'efficacité de l'implémentation, il faut mesurer le nombre de cycles processeurs et d'instructions utilisées lors de l'exécution d'une fonction, le patron du code pour les mesures est dans l'annexe A, on commence par exécuter un certain nombre de fois la fonction à mesurer pour que son code soit chargé dans les caches du processeur, puis on peut effectuer les chronométrages de la fonction. La technique utilisée pour ces chonométrages est fortement inspirée de \cite{intel}.


\subsubsection{Nombre de cycles pour les opérations élémentaires}
En utilisant le code  de chronométrage de l'annexe A, on obtient les résultats suivants sur un Intel Core i3-4130 :

\begin{figure}[H]
  \begin{center}
    \begin{tabular}{|| c | c | c | c ||}
      \hline
      Fonction & Nombre de cycles & Nombre d'instructions & IPC \\
      \hline\hline
      $\otimes$ (64 bits natif) & 7 & 14 & 2 \\
      \hline
      $\otimes$ (64 bits explicite) & 10 & 36 & 3.6 \\
      \hline
      $\otimes$ (128 bits) & 22 & 73 & 3.32 \\
      \hline
      $\otimes$ (256 bits) & 80 & 220 & 2.75 \\
      \hline
      $\otimes$ (512 bits) & 349 & 887 & 2.54 \\
      \hline\hline
      $\oplus$ (64 bits) & 7 & 12 & 1.71 \\
      \hline
      $\oplus$ (128 bits) & 7 & 17 & 2.43 \\
      \hline
      $\oplus$ (256 bits) & 20 & 42 & 2.1 \\
      \hline
      $\oplus$ (512 bits) & 42 & 78 & 1.86 \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Nombre d'instructions et de cycles de calculs pour l'addition et la multiplication}
\end{figure}

La diminution du nombre d'instructions par cycle lorsque l'on augmente la taille des entiers s'explique par le fait qu'une grande partie des opérations effectuées par le processeur consiste uniquement à charger les données dans les registres, un processeur Intel moderne faisant une opération \texttt{mov} par cycle, cela ralentit l'exécution.

%Sur un Raspberry Pi 2, j'ai également mesuré le nombre de cycles

\subsubsection{Chronométrage d'une multiplication modulaire}
L'intérêt de l'AMNS est d'effectuer des multiplications modulaires plus rapidement, ainsi, j'ai effectué des chronométrages sur différents AMNS, avec des tailles de $p$ variant. Pour ce faire, il a fallu modifier le générateur de code C pour utiliser la librairie d'arithmétique multiprécision.

\begin{figure}[H]
  \centerfloat
  \begin{tabular}{||c|c|c|c|c|c|c|c||}
      \hline
      \textit{\textbf{P-size}} & \textit{\textbf{AMNS - 64}} & \textit{\textbf{N - 64}} & \textit{\textbf{AMNS - 128}} & \textit{\textbf{N - 128}} & \textit{\textbf{GMP (Basic)}} & \textit{\textbf{OpenSSL}} & \textit{\textbf{OpenSSL (Montgommery)}} \\
      \hline\hline
      256 & 153 C. | 3.53 IPC & 5 & 384 C. | 3.42 IPC & 3 & 347 C. | 3.44 IPC & 1138 C. | 2.30 IPC & 143 C. | 3.67 IPC \\
      \hline
      300 & 245 C. | 3.57 IPC & 6 & 371 C. | 3.41 IPC & 3 & 483 C. | 3.24 IPC & 1474 C. | 2.13 IPC & 195 C. | 3.67 IPC \\
      \hline
      350 & 353 C. | 3.47 IPC & 7 & 367 C. | 3.43 IPC & 3 & 555 C. | 3.27 IPC & 1600 C. | 2.16 IPC & 261 C. | 3.64 IPC \\
      \hline
      400 & 592 C. | 3.37 IPC & 9 & 1035 C. | 3.33 IPC & 5 & 663 C. | 3.24 IPC & 1806 C. | 2.24 IPC & 340 C. | 3.59 IPC \\
      \hline
      450 & 532 C. | 3.36 IPC & 9 & 1124 C. | 3.34 IPC & 5 & 778 C. | 3.16 IPC & 1917 C. | 2.28 IPC & 440 C. | 3.25 IPC \\
      \hline
      500 & 900 C. | 3.25 IPC & 11 & 2228 C. | 3.26 IPC & 7 & 753 C. | 3.29 IPC & 2224 C. | 2.11 IPC & 295 C. | 3.31 IPC \\
      \hline
      650 & 1330 C. | 3.19 IPC & 13 & 2116 C. | 3.26 IPC & 7 & 1157 C. | 3.26 IPC & 3105 C. | 2.32 IPC & 777 C. | 3.45 IPC \\
      \hline
      750 & 1641 C. | 3.10 IPC & 15 & 2227 C. | 3.28 IPC & 7 & 1253 C. | 3.36 IPC & 3574 C. | 2.39 IPC & 633 C. | 2.94 IPC \\
      \hline
\end{tabular}
\end{figure}

On observe donc que l'utilisation d'une arithmétique multiprécision n'est pas concluante sur une architecture x86\_64, puisque le surcoût apporté par les manipulations d'entiers multiprécision est supérieur au gain dû à la diminution du degré des polynômes manipulés.

\subsubsection{Complexité théorique}

On peut également compter les opérations élémentaires (au niveau du processeur) utilisées lors d'une multiplication avec des coefficients de différentes tailles. Soit $p$ un nombre premier, on appelle $n_{64}$ le degré des polynômes de l'AMNS construit à partir de $p$ et en utilisant des coefficients de 64 bits. De même pour $n_{128}$ et $n{256}$.

Une fonction couramment utilisée est la réduction interne, elle intervient dans chaque multiplication mais aussi après un certain nombre d'addition, c'est donc un bon indicateur pour comparer les complexités selon la taille des coefficients utilisés.

\begin{figure}[H]
  \begin{center}
    \begin{tabular}{|| c | c | c | c | c | c | c | c ||}
      \hline
      Taille des coefficients & $\oplus$ & $\otimes$ & $\ominus$ & >> & AND & OR & NOT \\
      \hline\hline
      64 & $3 n_{64}^2$ & $2 n_{64}^2$ & $n_{64}^2$ & $2 n_{64}$ & 0 & 0 & 0 \\
      \hline
      128 & $14 n_{128}^2 + n_{128}$ & $7 n_{128}^2$ & $7 n_{128}^2$ & $2 n_{128}^2 + 4 n_{128}$ & $2 n_{128}^2$ & $3 n_{128}$ & $4 n_{128}^2$ \\
      %\hline
      %256 &  &  &  \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Nombre d'opérations élémentaires pour la réduction interne}
\end{figure}

Dans le cas optimal, on a $n_{64} = 2 n_{128}$, on peut donc prendre $n = n_{128}$, ce qui permet de réellement comparer les coûts.

\begin{figure}[H]
  \begin{center}
    \begin{tabular}{|| c | c | c | c | c | c | c | c ||}
      \hline
      Taille des coefficients & $\oplus$ & $\otimes$ & $\ominus$ & >> & AND & OR & NOT \\
      \hline\hline
      64 & $12 n^2$ & $8 n^2$ & $4 n^2$ & $4 n$ & 0 & 0 & 0 \\
      \hline
      128 & $14 n^2 + n$ & $7 n^2$ & $7 n^2$ & $2 n^2 + 4 n$ & $2 n^2$ & $3 n$ & $4 n^2$ \\
      %\hline
      %256 &  &  &  \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Nombre d'opérations élémentaires (normalisé) pour la réduction interne}
\end{figure}

La diminution du nombre de multiplications en utilisant des coefficients sur 128 bits est due au fait que l'on effectue une multiplication de polynômes modulo $2^{128}$ lors de la réduction interne, ce qui permet une optimisation. Cependant il y a beaucoup plus d'opérations élémentaires supplémentaires qui sont nécessaires à cause de la non-flexibilité du matériel.


\section{Réduction de réseau en norme infinie}
Lors de la génération d'un AMNS à partir d'un nombre premier $p$, pour calculer le polynôme $M$ utilisé dans la réduction interne, un réseau de polynômes est construit puis une base réduite en est calculée. En effet, on cherche le plus petit vecteur possible de ce réseau pour minimiser la norme infinie de $M$, puisque l'on a la relation  $\rho > 2n|\lambda|\,||M||_\infty$ \cite{Didier2020}. Préalablement, la méthode de réduction utilisée était l'algorithme LLL \cite{reduction} qui réduit la base selon la norme euclidienne. Or, dans la génération d'un AMNS, la norme utilisée lors des calculs de bornes avec les vecteurs de la base du réseau est la norme infinie. J'ai donc implémenté dans le langage Sage l'algorithme de réduction de base en norme infinie, qui est $\Delta$-LS \cite{reduction} (cet algorithme est valable pour n'importe quelle norme). Cependant, le calcul d'une distance entre un point et un réseau en norme infinie est très coûteux car il faut résoudre un problème d'optimisation. En pratique, l'utilisation de cet algorithme est entre 30 et 50 fois plus longue que l'utilisation de l'algorithme LLL. Cependant, en combinant les deux algorithmes, on obtient des performances comparables à celles de LLL et de meilleurs résultats qu'en utilisant seulement LLL ou $\Delta$-LS.

En particulier, pour $p = 78832353620269978920758519055100750909889117612453622437980854229007868033361$, en combinant les deux algorithmes, on peut générer un AMNS avec $n = 12$ et $E = X^{12} + 3$, alors qu'en utilisant uniquement LLL, on génère au mieux un AMNS avec $n = 13$ et $E = X^{13} - 2$. Cette approche permet donc de se rapprocher des limites théoriques définies dans \cite{Didier2020}, et d'obtenir des AMNS plus optimisés.

\section{Continuation}
Une grande limite de l'arithmétique multiprécision implémentée lors de ce stage est le matériel utilisé. En effet, les processeurs x86\_64 ne sont pas prévus pour effectuer de l'arithmétique multiprécision, (sauf en utilisant les instructions AVX, et versions suivantes), ainsi l'implantation d'une telle librairie uniquement en logiciel ne permet pas d'obtenir des gains substanciels. Une autre approche serait d'utiliser des circuit FPGA qui permettraient d'éliminer une partie du surcoût en opérations élémentaires. Il reste également à vérifier l'utilité d'une arithmétique multiprécision sur des processeurs ARM ne supportant pas le calcul d'une multiplication d'entiers en renvoyant un résultat sur 128 bits, ce qui pourrait donner un gain de performance plus élevé que de passer d'entiers 64 bits à des entiers 128 bits.
%\section{Conclusion}
%L'utilisation d'arithmétique multiprécision n'est donc pas forcément avantageuse sur les architectures Intel qui permettent d'obtenire les résultats de multiplications sur 128 bits directement, cependant, la question se pose pour les architectures ARM qui ne permettent de récupérer que la partie basse d'une multiplication, et force pour l'instant l'utilisation de coefficients de 32 bits.

\printbibliography

\appendix

\section{Mesure des fonctions élémentaires}
En considérant que la fonction à mesurer est \texttt{fonction\_test}, on utilise le protocole suivant, que l'on compile avec \texttt{gcc -o timing.exe -funroll-loops timing.c-lgmp -L.. -lintegers} :

\begin{lstlisting}[language=C]
#include <gmp.h>
#include <stdio.h>

#include "../convert.h"
#include "../integers.h"
#include "timing.h"

int main(void) {
  printf("Lancement des mesures...");

  // Initialisation
  printf("Initialisation...\n");
  gmp_randstate_t r_state;
  mpz_t A, B;

  mpz_inits(A, B, NULL);

  gmp_randinit_default(r_state);
  gmp_randseed_ui(r_state, get_seed());

  uint64_t timer, mean_timer, time_start, time_end;
  uint64_t a_timer, mean_a_timer, a_time_start, a_time_end;
  uint64_t n_ins, mean_ins, n_ins_start, n_ins_end;

  printf("\nOperation sur %d bits...\n", S_BASE);

  INT(S_BASE) a, b, c;
#ifdef T_DOUBLE
  INT(S_DOUBLE) d;
#endif

  /*------------------------- Mesure de la fonction -------------------------*/

  // Chauffage des caches
  for (int i = 0; i < N_TESTS; i++) {
#ifdef O_SINGLE
    c = fonction_test(a);
#else
#ifdef T_DOUBLE
    d = fonction_test(a, b);
#else
    c = fonction_test(a, b);
#endif
#endif
  }

  n_ins = rdpmc_instructions();
  printf("RDPMC : %lu\n", n_ins);

  for (int i = 0; i < N_SAMPLES; i++) {
    // Nouveau jeu de donnees
    mpz_urandomb(A, r_state, S_BASE - 1);
    mpz_urandomb(B, r_state, S_BASE - 1);

    a = FROM_MPZ(S_BASE)(A);
    b = FROM_MPZ(S_BASE)(B);

    // Calcul du nombre de cycles (CPU)
    timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      time_start = cpu_cycles_start();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
        c = fonction_test(a);
#else
#ifdef T_DOUBLE
        d = fonction_test(a, b);
#else
        c = fonction_test(a, b);
#endif
#endif
      }

      time_end = cpu_cycles_stop();

      if (timer > (time_end - time_start)) {
        timer = time_end - time_start;
      }
    }

    // Calcul du nombre de cycles (Actual)
    a_timer = -1;

    for (int j = 0; j < N_TESTS; j++) {
      a_time_start = rdpmc_actual_cycles();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
    c = fonction_test(a);
#else
#ifdef T_DOUBLE
        d = fonction_test(a, b);
#else
        c = fonction_test(a, b);
#endif
#endif
      }

      a_time_end = rdpmc_actual_cycles();

      if (a_timer > (a_time_end - a_time_start)) {
        a_timer = a_time_end - a_time_start;
      }
    }

    // Calcul du nombre d'instructions
    n_ins = -1;

    for (int j = 0; j < N_TESTS; j++) {
      n_ins_start = rdpmc_instructions();

      for (int k = 0; k < 100; k++) {
#ifdef O_SINGLE
    c = fonction_test(a);
#else
#ifdef T_DOUBLE
        d = fonction_test(a, b);
#else
        c = fonction_test(a, b);
#endif
#endif
      }

      n_ins_end = rdpmc_instructions();

      if (n_ins > (n_ins_end - n_ins_start)) {
        n_ins = n_ins_end - n_ins_start;
      }
    }

    mean_timer += timer;
    mean_a_timer += a_timer;
    mean_ins += n_ins;
  }

  printf("\n%s : %ld (TSC), %ld (Actual), %ld (Instructions), %f (IPC)\n",
         fonction_test, mean_timer / (N_SAMPLES * 100),
         mean_a_timer / (N_SAMPLES * 100), mean_ins / (N_SAMPLES * 100),
         (double)mean_ins / (double)mean_a_timer);

  // Vidage de la memoire
  mpz_clears(A, B, NULL);
  gmp_randclear(r_state);

  return 0;
}
\end{lstlisting}

\section{Réduction de réseau en norme infinie}
Soit $B = (b_1,\ldots,b_n)$ une base d'un réseau, on définit les fonctions $F_i(x) = \min_{z_k \in \mathbf{R}}|| x - \sum_{j = 1}^{i - 1} z_j * b_j||_\infty$ qui représentent les distances de $x$ au réseau engendré par $b_1,\ldots,b_{i-1}$. C'est ce qui est appelé \texttt{infinite\_distance} dans l'algorithme suivant. Pour calculer cette distance, il est nécessaire de résoudre un problème d'optimisation convexe, ce qui se fait relativement facilement avec sage. De même, pour le calcul de \texttt{mu\_j}, on résoud le problème $\min_{z_l \in \mathbf{R}}|| b_k - \mu * b_j - \sum_{i = 1}^{j - 1} z_i * b_i||_\infty$ avec $\mu$ entier, l'utilisation d'un solveur externe est ici aussi requise.

\begin{lstlisting}[language=Python]
def reduction_LS(b, delta):
    """
    Renvoie une base reduite de parametre delta selon l'algorithme de Lovasz et Scarf
    """
    k = 1
    m, n = b.shape

    while k < n:
        # Minimisation de b_k
        for j in range(k - 1, -1, -1):
            # Minimisation de mu
            mu_j = get_minimal_mu(b, k, j)

            # On modifie b_k
            b[k] -= mu_j * b[j]

        if infinite_distance(b[k], b, k - 1) < delta * infinite_distance(
            b[k - 1], b, k - 1
        ):
            # On echange les deux vecteurs et on recommence
            b[k - 1], b[k] = b[k].copy(), b[k - 1].copy()
            k = max(1, k - 1)
        else:
            k = k + 1

    return b
\end{lstlisting}


\end{document}
