#include "structs_data.h"

// Size of the parameters
#define S_BASE 64
#define S_DOUBLE 128

void add_poly(int64_t *rop, int64_t *pa, int64_t *pb);
void sub_poly(int64_t *rop, int64_t *pa, int64_t *pb);
void neg_poly(int64_t *rop, int64_t *op);
void scalar_mult_poly(int64_t *rop, int64_t *op, int64_t scalar);
void mult_mod_poly(int64_t *pa, int64_t *pb);

void from_int_to_amns(int64_t *rop, mpz_t op);
void from_amns_to_int(mpz_t rop, int64_t *op);

int log2_uint128(uint128 num);
int find_max_log2_coeff();
void internal_reduction_1(int64_t *rop, int64_t *op);
void internal_reduction_2(int64_t *rop);
void red_int(int64_t *op);

void init_datas();
void free_datas();

void check_convertion(mpz_t A);
void check_add(mpz_t A, mpz_t B);
void check_mult(mpz_t A, mpz_t B);

void bench(int nbiter);

// Test new int128_t type
void mult_mod_poly_2(int64_t* pa, int64_t* pb);
void internal_reduction_1_2(int64_t* rop, int64_t* op);
int find_max_log2_coeff_2();
void internal_reduction_2_2(int64_t* rop);
