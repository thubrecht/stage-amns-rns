Arithmétique multiprécision pour les systèmes AMNS et RNS
=========================================================

# Compilation des tests de temps
Il faut effectuer :
```bash
make timing
```

Pour lancer ces tests, il est nécessaire d'avoir installé le paquet `msr-tools`.

Il faut d'abord charger le module dans le noyau :
```
sudo modprobe msr
```

Ensuite, il faut passer `root` et lancer les commandes suivantes :
```
echo "-1" > /proc/sys/kernel/perf_event_paranoid

echo 2 | dd of=/sys/devices/cpu/rdpmc
echo 2 | dd of=/sys/bus/event_source/devices/cpu/rdpmc

wrmsr -a 0x38d 0x0333
```

Pour désactiver en plus l'hyperthreading :
```
echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
```
